var app = (function(document) {

	'use strict';
	var docElem = document.documentElement,
        iphone = -1,
        ipad = -1,
        android = -1,

    _validName = function(name) {
        var re = /^([a-zA-Z ]){2,30}$/;
        return re.test(name);
    },

    _validEmail = function(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    },

    _validPostCode = function(postcode) {
        postcode = postcode.replace(/\s/g, "");
        var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
        return regex.test(postcode);
    },

    _getContentMasterClass = function(aName, aEmail, aOrganization, aTelephone, aQuery, aRequestType) {
        // {name : aName, telephone: aTelephone, postcode: aPostcode}
        jQuery.support.cors = true;
        if(aRequestType == 'contact_request'){
            $('#skyval-contact-full').fadeOut();
            $('#skyval-contact-thanks').fadeOut();
            $('#skyval-contact-spinner').fadeIn();
        }
        $.ajax({
            url: '/sendMail?name=' + aName + '&email=' + aEmail + '&organization=' + aOrganization + '&telephone=' + aTelephone + '&query=' + aQuery + '&request_type=' + aRequestType,
            type: 'POST',
            contentType: 'text/plain',
            dataType: 'json',
            crossDomain: true,
            async: true,
            data: '',
            xhrFields: {
                // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
                // This can be used to set the 'withCredentials' property.
                // Set the value to 'true' if you'd like to pass cookies to the server.
                // If this is enabled, your server must respond with the header
                // 'Access-Control-Allow-Credentials: true'.
                withCredentials: false
            },
            headers: {
                // Set any custom headers here.
                // If you set any non-simple headers, your server must include these
                // headers in the 'Access-Control-Allow-Headers' response header.
            },
            beforeSend: function() {
                //$('#'+id+' .contentarea').html('<img src="/function-demos/functions/ajax/images/loading.gif" />');
            },
            success: function(data, textStatus, xhr) {
                if(aRequestType == 'contact_request'){

                    if(aRequestType == 'contact_request'){
                        $('#skyval-contact-spinner').fadeOut();
                        setTimeout(function() {
                            $('#main-contact-container').css("height", "");
                            $('#skyval-contact-thanks').fadeIn();
                        }, 500);
                    }
                }

                if(aRequestType == 'demo_request'){
                    var
                        loader = $('.modal-loader'),
                        thanks = $('.modal-thanks');
                    thanks.height(loader.height());

                    loader.fadeOut();
                    setTimeout(function() {
                        thanks.fadeIn();
                    }, 500);
                }
            },
            error: function(xhr, textStatus, errorThrown) {}
        });
    },

	_userAgentInit = function() {
		docElem.setAttribute('data-useragent', navigator.userAgent);
	},

	_skyvalInit = function() {
		$('a[data-scroll_to]').click(function(e) {
	        $('html,body').animate({ scrollTop: $(this.hash).offset().top - $('#header').height()}, 1000);
	        return false;
	        e.preventDefault();
	    });
	},

	_fancyboxInit = function() {
		// $('.fancybox').fancybox();
	},

    _detectMobile = function(){
        app.iphone = -1 != navigator.userAgent.indexOf("iPhone") || -1 != navigator.userAgent.indexOf("iPod");
        app.ipad = -1 != navigator.userAgent.indexOf("iPad");
        app.android = -1 != navigator.userAgent.indexOf("Android");

        if(app.iphone || app.ipad || app.android) {
            $("a").removeAttr("data-target");
            $("a").removeAttr("data-toggle");
        }
    },

    _videoJSInit = function() {
        videojs("shortBAEVideo").ready(function(){
            var shortBAEVideo = this;

            $('#nigelVideo').on('shown.bs.modal', function () {
                shortBAEVideo.play();
            });
            $('#nigelVideo').on('hidden.bs.modal', function () {
                shortBAEVideo.pause();
            });
        });
        videojs("longBAEVideo").ready(function(){
            var longBAEVideo = this;

            $('#introLongVideo').on('shown.bs.modal', function () {
                longBAEVideo.play();
            });
            $('#introLongVideo').on('hidden.bs.modal', function () {
                longBAEVideo.pause();
            });
        });

        videojs("promotionalVideo").ready(function(){
            var longBAEVideo = this;

            $('#introVideo').on('shown.bs.modal', function () {
                longBAEVideo.play();
            });
            $('#introVideo').on('hidden.bs.modal', function () {
                longBAEVideo.pause();
            });
        });

    },

	_carouserInit = function() {
		$('.carousel').carousel({
			 swipe: 30
		});
    	$(window).bind("load resize",function(e){
			var maxHeight = Math.max.apply(null, $("#inside .carousel .carousel-inner").map(function () {
	    		return $(this).height();
	    	}).get());
			if ($(window).width() > 960) {
	    		$('#inside').css('height', maxHeight+160);
	    	} else if ($(window).width() > 660) {
	    		$('#inside').css('height', maxHeight+100);
	    	} else {
	    		$('#inside').css('height', maxHeight+115);
	    	}
		});
	},

    _modalForm = function () {
        var
            form = $('.demo_form');

        form.on('submit', function (e) {
            e.preventDefault();
            var
                loader = $('.modal-loader'),
                thanks = $('.modal-thanks');

            e.preventDefault();
            var error = 0;
            $(this).find('input, select').each(function() {
                $(this).removeClass('has-error').parent().removeClass('has-error');
                if ($(this).hasClass('nameField')) {
                    if (!_validName($(this).val())) {
                        error = true;
                        $(this).parent().addClass('has-error');
                    }
                }
                if ($(this).hasClass('emailField')) {
                    if (!_validEmail($(this).val())) {
                        error = true;
                        $(this).parent().addClass('has-error');
                    }
                }
            });

            if (!error) {
                error = false;

                var name = $("input#skyval-demo-name").val();
                var email = $("input#skyval-demo-email").val();
                var telephone = $("input#skyval-demo-telephone").val();
                var query = $("textarea#skyval-demo-descr").val();

                var request_type = 'demo_request';
                var form = $(this);
                var url = 'contact_request_dev.php?name=' + name + '&email=' + email + '&organization=' + '' + '&telephone=' + telephone + '&query' + query + '&request_type=' + request_type;

                $(this).fadeOut();
                loader.fadeIn();

                _getContentMasterClass(name, email, '', telephone, query, request_type);
            }
        });
    },

    _contactModalForm = function () {
        var contactForm = $('#skyval-contact');

        contactForm.on('submit', function (e) {
            e.preventDefault();
            var error = 0;
            var
                loader = $('#skyval-contact-spinner'),
                thanks = $('#skyval-contact-thanks'),
                mainForm = $('#skyval-contact');

            $(this).find('input, select').each(function() {
                $(this).removeClass('has-error').parent().removeClass('has-error');
                if ($(this).hasClass('nameField')) {
                    if (!_validName($(this).val())) {
                        error = true;
                        $(this).parent().addClass('has-error');
                    }
                }
                if ($(this).hasClass('emailField')) {
                    if (!_validEmail($(this).val())) {
                        error = true;
                        $(this).parent().addClass('has-error');
                    }
                }
            });

            if (!error) {
                error = false;

                var name = $("input#skyval-name").val();
                var organization = $("input#skyval-organization").val();
                var email = $("input#skyval-email").val();
                var telephone = $("input#skyval-telephone").val();
                var query = $("textarea#skyval-query").val();
                var request_type = 'contact_request';
                var form = $(this);
                var url = 'contact_request_dev.php?name=' + name + '&email=' + email + '&organization=' + organization + '&telephone=' + telephone + '&query' + query + '&request_type=' + request_type;

                var mainContactContainer = $('#main-contact-container').height();
                $('#main-contact-container').height(mainContactContainer);

                mainForm.hide();

                _getContentMasterClass(name, email, organization, telephone, query, request_type);
            }

        });

    },

    _init = function() {

        _skyvalInit();

        // User Agent workaround
        _userAgentInit();

        _carouserInit();

        _fancyboxInit();

        _detectMobile();

        if(!(app.iphone || app.ipad || app.android)){
            _videoJSInit();
        } else {
            if($('*').is('#shortBAEVideo')) {
                videojs("shortBAEVideo").dispose();
            }
            if($('*').is('#longBAEVideo')) {
                videojs("longBAEVideo").dispose();
            }
            if($('*').is('#promotionalVideo')) {
                videojs("promotionalVideo").dispose();
            }
        }

        _modalForm();

        _contactModalForm();
    };


	return {
		init: _init
	};

})(document, $);

(function() {

	'use strict';
	app.init();

})();

