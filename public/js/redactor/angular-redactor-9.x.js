if (!RedactorPlugins) var RedactorPlugins = {};
(function() {
    'use strict';

    /**
     * usage: <textarea ng-model="content" redactor></textarea>
     *
     *    additional options:
     *      redactor: hash (pass in a redactor options hash)
     *
     */

    var redactorOptions = {};

    angular.module('angular-redactor', [])
        .constant('redactorOptions', redactorOptions)
        .directive('redactor', ['$timeout', function($timeout) {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {
                    // Expose scope var with loaded state of Redactor
                    scope.redactorLoaded = false;
                    var editor;
                    var updateModel = function updateModel(value) {
                            // $timeout to avoid $digest collision
                            $timeout(function() {
                                scope.$apply(function() {
                                    ngModel.$setViewValue(value);
                                });

                                var data = element.redactor('code.get');
                                data = element.redactor('clean.stripTags',data);
                                if(data.length < attrs.ngMaxlength){
                                    scope.MAX_LENGTH_EDITOR_VALID = false;
                                } else {
                                    scope.MAX_LENGTH_EDITOR_VALID = true;
                                }

                            });
                        },
                        options = {
                            changeCallback: updateModel,
                            blurCallback: function(e) {
                                var data = element.redactor('code.get');
                                data = element.redactor('clean.stripTags',data);
                                if(!data){
                                    this.focus.setEnd();
                                }
                            },
                            imageManagerJson: 'files/upload',
                            plugins: ['imagemanager']
                        },
                        additionalOptions = attrs.redactor ?
                            scope.$eval(attrs.redactor) : {}, editor;

                    angular.extend(options, redactorOptions, additionalOptions);

                    // prevent collision with the constant values on ChangeCallback
                    if(!angular.isUndefined(redactorOptions.changeCallback)) {
                        options.changeCallback = function() {
                            updateModel.call(this);
                            redactorOptions.changeCallback.call(this);
                        }
                    }
                    // put in timeout to avoid $digest collision.  call render() to
                    // set the initial value.
                    $timeout(function() {

                        $.ajax({
                            dataType: "json",
                            method: 'GET',
                            async: false,
                            cache: false,
                            url: 'api/token',
                            success: $.proxy(function(data) {
                                scope.token = data.token;
                            }, this)
                        });

                        options.imageUpload = 'files/upload?_token='+scope.token+'&issue_key=' + scope.issue_key;
                        editor = element.redactor(options);
                        scope.REDACTOR_ELEMENT = element;
                        ngModel.$render();
                    });

                    ngModel.$render = function() {
                        if(angular.isDefined(editor)) {
                            $timeout(function() {
                                if(ngModel.$viewValue){
                                    element.redactor('insert.set', ngModel.$viewValue || '');
                                } else {
                                    element.redactor('insert.html', ngModel.$viewValue || '');
                                }
                                scope.redactorLoaded = true;
                            });
                        }
                    };

                    RedactorPlugins.imagemanager = function()
                    {
                        return {
                            langs: {
                                en: {
                                    "upload": "Upload",
                                    "choose": "Choose"
                                }
                            },
                            init: function()
                            {
                                if (!this.opts.imageManagerJson)
                                {
                                    return;
                                }

                                this.modal.addCallback('image', this.imagemanager.load);
                            },
                            load: function()
                            {
                                var $box = $('<div style="overflow: auto; height: 300px;" class="redactor-modal-tab" data-title="Choose">');
                                this.modal.getModal().append($box);

                                $.ajax({
                                    dataType: "json",
                                    cache: false,
                                    data:{'issue_key': scope.issue_key, 'type': "images"},
                                    url: this.opts.imageManagerJson,
                                    success: $.proxy(function(data)
                                    {
                                        $.each(data, $.proxy(function(key, val)
                                        {
                                            // title
                                            var thumbtitle = '';
                                            if (typeof val.name !== 'undefined') {
                                                thumbtitle = val.title;
                                            }

                                            var img = $('<img src="' + val.path + '" rel="' + val.path + '" title="' + thumbtitle + '" style="width: 100px; height: 75px; cursor: pointer;" />');
                                            $box.append(img);
                                            $(img).click($.proxy(this.imagemanager.insert, this));

                                        }, this));

                                    }, this)
                                });
                            },
                            insert: function(e)
                            {
                                var $el = $(e.target);

                                var img = '<img src="'+$el.attr('rel')+'"';
                                var alt = 'alt="'+$el.attr('title')+'" ';
                                var title = 'title="'+$el.attr('title')+'" >';
                                this.insert.html(img+alt+title);//('insert.html', img+alt+title);
                            }
                        };
                    };

                }
            };
        }]);
})();

