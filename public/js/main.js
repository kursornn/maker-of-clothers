+function ($) {
  'use strict';

  if ( !$.fn.carousel ) {
    throw new Error( "carousel-swipe required bootstrap carousel" )
  }

  // CAROUSEL CLASS DEFINITION
  // =========================

  var CarouselSwipe = function(element) {
    this.$element    = $(element)
    this.carousel    = this.$element.data('bs.carousel')
    this.options     = $.extend({}, CarouselSwipe.DEFAULTS, this.carousel.options)
    this.startX      =
    this.startY      =
    this.startTime   =
    this.cycling     =
    this.$active     =
    this.$items      =
    this.$next       =
    this.$prev       =
    this.dx          = null

    this.$element
      .on('touchstart', $.proxy(this.touchstart,this))
      .on('touchmove', $.proxy(this.touchmove,this))
      .on('touchend', $.proxy(this.touchend,this))
  }

  CarouselSwipe.DEFAULTS = {
    swipe: 50 // percent per second
  }

  CarouselSwipe.prototype.touchstart = function(e) {
    if (!this.options.swipe) return;
    var touch = e.originalEvent.touches ? e.originalEvent.touches[0] : e
    this.dx = 0
    this.startX = touch.pageX
    this.startY = touch.pageY
    this.cycling = null
    this.width = this.$element.width()
    this.startTime = e.timeStamp
  }

  CarouselSwipe.prototype.touchmove = function(e) {
    if (!this.options.swipe) return;
    var touch = e.originalEvent.touches ? e.originalEvent.touches[0] : e
    var dx = touch.pageX - this.startX
    var dy = touch.pageY - this.startY
    if (Math.abs(dx) < Math.abs(dy)) return; // vertical scroll

    if ( this.cycling === null ) {
      this.cycling = !!this.carousel.interval
      this.cycling && this.carousel.pause()
    }

    e.preventDefault()
    this.dx = dx / (this.width || 1) * 100
    this.swipe(this.dx)
  }

  CarouselSwipe.prototype.touchend = function(e) {
    if (!this.options.swipe) return;
    if (!this.$active) return; // nothing moved
    var all = $()
      .add(this.$active).add(this.$prev).add(this.$next)
      .carousel_transition(true)

    var dt = (e.timeStamp - this.startTime) / 1000
    var speed = Math.abs(this.dx / dt) // percent-per-second
    if (this.dx > 40 || (this.dx > 0 && speed > this.options.swipe)) {
      this.carousel.prev()
    } else if (this.dx < -40 || (this.dx < 0 && speed > this.options.swipe)) {
      this.carousel.next();
    } else {
      this.$active
        .one($.support.transition.end, function () {
          all.removeClass('prev next')
        })
      .emulateTransitionEnd(this.$active.css('transition-duration').slice(0, -1) * 1000)
    }

    all.css('transform', '')
    this.cycling && this.carousel.cycle()
    this.$active = null // reset the active element
  }

  CarouselSwipe.prototype.swipe = function(percent) {
    var $active = this.$active || this.getActive()
    if (percent < 0) {
        this.$prev
            .css('transform', 'translate3d(0,0,0)')
            .removeClass('prev')
            .carousel_transition(true)
        if (!this.$next.length || this.$next.hasClass('active')) return
        this.$next
            .carousel_transition(false)
            .addClass('next')
            .css('transform', 'translate3d(' + (percent + 100) + '%,0,0)')
    } else {
        this.$next
            .css('transform', '')
            .removeClass('next')
            .carousel_transition(true)
        if (!this.$prev.length || this.$prev.hasClass('active')) return
        this.$prev
            .carousel_transition(false)
            .addClass('prev')
            .css('transform', 'translate3d(' + (percent - 100) + '%,0,0)')
    }

    $active
        .carousel_transition(false)
        .css('transform', 'translate3d(' + percent + '%, 0, 0)')
  }

  CarouselSwipe.prototype.getActive = function() {
    this.$active = this.$element.find('.item.active')
    this.$items = this.$active.parent().children()

    this.$next = this.$active.next()
    if (!this.$next.length && this.options.wrap) {
      this.$next = this.$items.first();
    }

    this.$prev = this.$active.prev()
    if (!this.$prev.length && this.options.wrap) {
      this.$prev = this.$items.last();
    }

    return this.$active;
  }

  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  var old = $.fn.carousel
  $.fn.carousel = function() {
    old.apply(this, arguments);
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel.swipe')
      if (!data) $this.data('bs.carousel.swipe', new CarouselSwipe(this))
    })
  }

  $.extend($.fn.carousel,old);

  $.fn.carousel_transition = function(enable) {
    enable = enable ? '' : 'none';
    return this.each(function() {
      $(this)
        .css('-webkit-transition', enable)
        .css('transition', enable)
    })
  };

}(jQuery);

var app = (function(document) {

	'use strict';
	var docElem = document.documentElement,
        iphone = -1,
        ipad = -1,
        android = -1,

    _validName = function(name) {
        var re = /^([a-zA-Z ]){2,30}$/;
        return re.test(name);
    },

    _validEmail = function(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    },

    _validPostCode = function(postcode) {
        postcode = postcode.replace(/\s/g, "");
        var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
        return regex.test(postcode);
    },

    _getContentMasterClass = function(aName, aEmail, aOrganization, aTelephone, aQuery, aRequestType) {
        // {name : aName, telephone: aTelephone, postcode: aPostcode}
        jQuery.support.cors = true;
        if(aRequestType == 'contact_request'){
            $('#skyval-contact-full').fadeOut();
            $('#skyval-contact-thanks').fadeOut();
            $('#skyval-contact-spinner').fadeIn();
        }
        $.ajax({
            url: '/sendMail?name=' + aName + '&email=' + aEmail + '&organization=' + aOrganization + '&telephone=' + aTelephone + '&query=' + aQuery + '&request_type=' + aRequestType,
            type: 'POST',
            contentType: 'text/plain',
            dataType: 'json',
            crossDomain: true,
            async: true,
            data: '',
            xhrFields: {
                // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
                // This can be used to set the 'withCredentials' property.
                // Set the value to 'true' if you'd like to pass cookies to the server.
                // If this is enabled, your server must respond with the header
                // 'Access-Control-Allow-Credentials: true'.
                withCredentials: false
            },
            headers: {
                // Set any custom headers here.
                // If you set any non-simple headers, your server must include these
                // headers in the 'Access-Control-Allow-Headers' response header.
            },
            beforeSend: function() {
                //$('#'+id+' .contentarea').html('<img src="/function-demos/functions/ajax/images/loading.gif" />');
            },
            success: function(data, textStatus, xhr) {
                if(aRequestType == 'contact_request'){

                    if(aRequestType == 'contact_request'){
                        $('#skyval-contact-spinner').fadeOut();
                        setTimeout(function() {
                            $('#main-contact-container').css("height", "");
                            $('#skyval-contact-thanks').fadeIn();
                        }, 500);
                    }
                }

                if(aRequestType == 'demo_request'){
                    var
                        loader = $('.modal-loader'),
                        thanks = $('.modal-thanks');
                    thanks.height(loader.height());

                    loader.fadeOut();
                    setTimeout(function() {
                        thanks.fadeIn();
                    }, 500);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
            }
        });
    },

	_userAgentInit = function() {
		docElem.setAttribute('data-useragent', navigator.userAgent);
	},

	_skyvalInit = function() {
		$('a[data-scroll_to]').click(function(e) {
	        $('html,body').animate({ scrollTop: $(this.hash).offset().top - $('#header').height()}, 1000);
	        return false;
	        e.preventDefault();
	    });
	},

	_fancyboxInit = function() {
		// $('.fancybox').fancybox();
	},

    _detectMobile = function(){
        app.iphone = -1 != navigator.userAgent.indexOf("iPhone") || -1 != navigator.userAgent.indexOf("iPod");
        app.ipad = -1 != navigator.userAgent.indexOf("iPad");
        app.android = -1 != navigator.userAgent.indexOf("Android");

        if(app.iphone || app.ipad || app.android) {
            $(".videojs-delitable").removeAttr("data-target");
            $(".videojs-delitable").removeAttr("data-toggle");
        }
    },

    _videoJSInit = function() {
        if($('*').is('#shortBAEVideo')) {
            videojs("shortBAEVideo").ready(function(){
                var shortBAEVideo = this;

                $('#nigelVideo').on('shown.bs.modal', function () {
                    shortBAEVideo.play();
                });
                $('#nigelVideo').on('hidden.bs.modal', function () {
                    shortBAEVideo.pause();
                });
            });
        }
        if($('*').is('#longBAEVideo')) {
            videojs("longBAEVideo").ready(function(){
                var longBAEVideo = this;

                $('#introLongVideo').on('shown.bs.modal', function () {
                    longBAEVideo.play();
                });
                $('#introLongVideo').on('hidden.bs.modal', function () {
                    longBAEVideo.pause();
                });
            });
        }
        if($('*').is('#promotionalVideo')) {
            videojs("promotionalVideo").ready(function(){
                var longBAEVideo = this;

                $('#introVideo').on('shown.bs.modal', function () {
                    longBAEVideo.play();
                });
                $('#introVideo').on('hidden.bs.modal', function () {
                    longBAEVideo.pause();
                });
            });
        }
    },

	_carouserInit = function() {
        $('.carousel').carousel({
			 swipe: 30
		});

        $(window).bind("load resize",function(e){
            _resizeSlide();
		});

        $(window).bind("slid.bs.carousel", function(e){
            _resizeSlide();
        });

        var _resizeSlide = function(){
            var maxHeight = Math.max.apply(null, $("#inside .carousel .carousel-inner").map(function () {
                return $(this).height();
            }).get());

            if ($(window).width() > 960) {
                $('#inside').css('height', maxHeight+160);
            } else if ($(window).width() > 660) {
                $('#inside').css('height', maxHeight+100);
            } else {
                $('#inside').css('height', maxHeight+115);
            }
        }
	},

    _modalForm = function () {
        var
            form = $('.demo_form');

        form.on('submit', function (e) {
            e.preventDefault();
            var
                loader = $('.modal-loader'),
                thanks = $('.modal-thanks');

            e.preventDefault();
            var error = 0;
            $(this).find('input, select').each(function() {
                $(this).removeClass('has-error').parent().removeClass('has-error');
                if ($(this).hasClass('nameField')) {
                    if (!_validName($(this).val())) {
                        error = true;
                        $(this).parent().addClass('has-error');
                    }
                }
                if ($(this).hasClass('emailField')) {
                    if (!_validEmail($(this).val())) {
                        error = true;
                        $(this).parent().addClass('has-error');
                    }
                }
            });

            if (!error) {
                error = false;

                var name = $("input#skyval-demo-name").val();
                var email = $("input#skyval-demo-email").val();
                var telephone = $("input#skyval-demo-telephone").val();
                var query = $("textarea#skyval-demo-descr").val();

                var request_type = 'demo_request';
                var form = $(this);
                var url = 'contact_request_dev.php?name=' + name + '&email=' + email + '&organization=' + '' + '&telephone=' + telephone + '&query' + query + '&request_type=' + request_type;

                $(this).fadeOut();
                loader.fadeIn();

                _getContentMasterClass(name, email, '', telephone, query, request_type);
            }
        });
    },

    _contactModalForm = function () {
        var contactForm = $('#skyval-contact');

        contactForm.on('submit', function (e) {
            e.preventDefault();
            var error = 0;
            var
                loader = $('#skyval-contact-spinner'),
                thanks = $('#skyval-contact-thanks'),
                mainForm = $('#skyval-contact');

            $(this).find('input, select').each(function() {
                $(this).removeClass('has-error').parent().removeClass('has-error');
                if ($(this).hasClass('nameField')) {
                    if (!_validName($(this).val())) {
                        error = true;
                        $(this).parent().addClass('has-error');
                    }
                }
                if ($(this).hasClass('emailField')) {
                    if (!_validEmail($(this).val())) {
                        error = true;
                        $(this).parent().addClass('has-error');
                    }
                }
            });

            if (!error) {
                error = false;

                var name = $("input#skyval-name").val();
                var organization = $("input#skyval-organization").val();
                var email = $("input#skyval-email").val();
                var telephone = $("input#skyval-telephone").val();
                var query = $("textarea#skyval-query").val();
                var request_type = 'contact_request';
                var form = $(this);
                var url = 'contact_request_dev.php?name=' + name + '&email=' + email + '&organization=' + organization + '&telephone=' + telephone + '&query' + query + '&request_type=' + request_type;

                var mainContactContainer = $('#main-contact-container').height();
                $('#main-contact-container').height(mainContactContainer);

                mainForm.hide();

                _getContentMasterClass(name, email, organization, telephone, query, request_type);
            }

        });

    },
    _runDemoModalForHashTag = function(){
        $(document).ready(function(e){
            var demoForm = $(".demo_form");
            var requiredHashTag = "#request_demo";
            var currentHashTag = window.location.hash;

            if(currentHashTag && requiredHashTag == currentHashTag) {
                demoForm.click();
            }
        });
    },
	_init = function() {
		_skyvalInit();

		// User Agent workaround
		_userAgentInit();

		_carouserInit();

		_fancyboxInit();
		
        _detectMobile();

         if(!(app.iphone || app.ipad || app.android)){
             _videoJSInit();
         } else {
             if($('*').is('#shortBAEVideo')) {
                 videojs("shortBAEVideo").dispose();
             }
             if($('*').is('#longBAEVideo')) {
                 videojs("longBAEVideo").dispose();
             }
             if($('*').is('#promotionalVideo')) {
                 videojs("promotionalVideo").dispose();
             }
        }


		_modalForm();

        _contactModalForm();

        _runDemoModalForHashTag();
	};


	return {
		init: _init
	};

})(document, $);

(function() {

	'use strict';
	app.init();

})();

