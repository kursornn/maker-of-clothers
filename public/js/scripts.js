(function () {

    $("#submit-order-row").hide();

    $(function () {

        $(document).on('change', '.preview-uploader input', function (event) {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(event.target).siblings('label').addClass('loaded')
                        .css('background-image', 'url(' + e.target.result + ')');
                };
                reader.readAsDataURL(this.files[0]);
            }
        });

        $('#campaigns, .js_select2').select2();

        $('#new-post-content').on('keyup', setTweetCount);
        if ($('#new-post-content').length) {
            setTweetCount.apply(document.getElementById('new-post-content'));
        }


        $('.js_datepicker').datetimepicker({
            sideBySide: false,
            format: "D MMMM YYYY",
            defaultDate: new Date()
        });

        $('#filter').on('submit', function (e) {

            var query = {
                campaigns: []
            };

            $.each($(this).serializeArray(), function (i, val) {

                if (val.name == "campaigns[]") {
                    query.campaigns.push(val.value);
                } else {
                    query[val.name] = val.value;
                }

            });

            query.campaigns = query.campaigns.join(',');
            //query.excluded  = query.excluded.join(',');

            //if(!query.unpublished) {
            //    query.unpublished = 0;
            //}

            if (!query.campaigns.length) {
                delete query.campaigns;
            }

            if (!query.search.length) {
                delete query.search;
            }

            if (Object.keys(query).length) {
                window.location.href = "/admin/posts?" + $.param(query);
            } else {
                window.location.href = "/admin/posts";
            }

            e.preventDefault();
            return false;
        });

        $('#active_filt').on('click', function (e) {
            e.preventDefault();
            window.location.href = "/campaigns?archived=0";
        });
        $('#archive_filt').on('click', function (e) {
            e.preventDefault();
            window.location.href = "/campaigns?archived=1";
        });


    });


    $('.checkbox-check').on('submit', function (e) {
        var $check = $(this).find('input.confirm');
        var $label = $check.parent();
        if($check.prop('checked') == false) {
            $label.addClass('shake');
            setTimeout(function () {
                $label.removeClass('shake');
            }, 1000);
            e.preventDefault();
            return false;
        }
    });


    function getTweetLength(text) {
        var linksSize = linkify.find(text).length * 23;
        var textWithLinks = text.linkify();
        textWithLinks = textWithLinks.replace(/<a(\s[^>]*)?>.*?<\/a>/ig, "");
        return (textWithLinks.length + linksSize);
    }

    function setTweetCount() {
        var message = $(this).val();
        var len = getTweetLength(message);
        $('#char-count').text(len);
        if (len > 140) {
            $('#char-count-block').addClass('oversized');
        } else {
            $('#char-count-block').removeClass('oversized');
        }
    }

    $('#new-post-content').redactor({
        buttons: ['html','formatting', 'bold', 'italic', 'link'],
        formatting: ['blockquote'],
        minHeight: 300,
        toolbarFixed: false,
        linebreaks: true,
        paragraphy: false
    });

    $("#select-level-up-panel1,#select-dress-level-up-panel1, #select-level-up-panel2, #select-dress-level-up-panel2" +
        ",#select-skirt-level-up-panel1, #select-skirt-level-up-panel2, #select-trousers-level-down-panel2" +
        ",#select-style-dress-level-up-panel1,#select-style-dress-level-up-panel2" +
        ",#select-shirt-level-up-panel1, #select-shirt-level-up-panel2" +
        ",#select-trousers-level-up-panel1, #select-trousers-level-up-panel2").imagepicker({
        hide_select : true,
        show_label  : true
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if(target === "#dress-level-up-panel4"){
            var dress_id = $("#select-dress-level-up-panel1").val();
            var style_up = $("#select-style-dress-level-up-panel1").val();
            var style_down = $("#select-style-dress-level-up-panel2").val();
            var size = $("#size-dress-level-up-panel3").val();
            var data = {"macro_type": 0, "tissue" : dress_id, "style_up": style_up, "style_down": style_down, "size": size};

            $("#macro_type-assign").val(0);
            $("#tissue-assign").val(dress_id);
            $("#style-up-assign").val(style_up);
            $("#style-down-assign").val(style_down);
            $("#size-assign").val(size);

            $.ajax({
                url: "/dashboard/corresponding/preview",
                data: data
            }).done(function(data) {
                if(data){
                    $("#dress-error-message").hide();
                    $("#dress-preview").attr("src", "/media/corresponding/" + data["corresponding"]["photo"]);
                    $("#preview-cost").val(data["cost"]);
                    $("#dress-preview-message").show();
                } else{
                    $("#dress-error-message").show();
                    $("#dress-preview-message").hide();
                }
            });
        }

        if(target == "#skirt-level-up-panel4"){
            var dress_id = $("#select-level-up-panel1").val();
            var style_up = $("#select-skirt-level-up-panel2").val();
            var style_down = $("#select-skirt-level-up-panel2").val();
            var size = $("#skirt-sizes-level-up-panel3").val();
            var data = {"macro_type": 1, "tissue" : dress_id, "style_up": style_up, "style_down": style_down, "size": size};

            $("#macro_type-assign").val(1);
            $("#tissue-assign").val(dress_id);
            $("#style-up-assign").val(style_up);
            $("#style-down-assign").val(style_down);
            $("#size-assign").val(size);

            $.ajax({
                url: "/dashboard/corresponding/preview",
                data: data
            }).done(function(data) {
                if(data){
                    $("#skirts-error-message").hide();
                    $("#skirt-preview").attr("src", "/media/corresponding/" + data["corresponding"]["photo"]);
                    $("#preview-cost").val(data["cost"]);
                    $("#skirts-preview-message").show();
                } else{
                    $("#skirts-error-message").show();
                    $("#skirts-preview-message").hide();
                }
            });
        }

        if(target == "#trousers-level-up-panel4"){
            var dress_id = $("#select-level-up-panel1").val();
            var style_up = $("#select-trousers-level-up-panel2").val();
            var style_down = $("#select-trousers-level-down-panel2").val();
            var size = $("#trousers-size-panel3").val();
            var data = {"macro_type": 2, "tissue" : dress_id, "style_up": style_up, "style_down": style_down, "size": size};

            $("#macro_type-assign").val(2);
            $("#tissue-assign").val(dress_id);
            $("#style-up-assign").val(style_up);
            $("#style-down-assign").val(style_down);
            $("#size-assign").val(size);

            $.ajax({
                url: "/dashboard/corresponding/preview",
                data: data
            }).done(function(data) {
                if(data){
                    $("#trousers-error-message").hide();
                    $("#trousers-preview").attr("src", "/media/corresponding/" + data["corresponding"]["photo"]);
                    $("#preview-cost").val(data["cost"]);
                    $("#trousers-preview-message").show();
                } else{
                    $("#trousers-error-message").show();
                    $("#trousers-preview-message").hide();
                }
            });
        }

        if(target == "#type-of-element-panel4"){
            var dress_id = $("#select-level-up-panel1").val();
            var style_up = $("#select-shirt-level-up-panel1").val();
            var style_down = $("#select-shirt-level-up-panel2").val();
            var size = $("#shirt-size-level-up-panel3").val();
            var data = {"macro_type": 3, "tissue" : dress_id, "style_up": style_up, "style_down": style_down, "size": size};

            $("#macro_type-assign").val(3);
            $("#tissue-assign").val(dress_id);
            $("#style-up-assign").val(style_up);
            $("#style-down-assign").val(style_down);
            $("#size-assign").val(size);

            $.ajax({
                url: "/dashboard/corresponding/preview",
                data: data
            }).done(function(data) {
                console.log(data);
                if(data){
                    $("#shirt-error-message").hide();
                    $("#shirt-preview").attr("src", "/media/corresponding/" + data["corresponding"]["photo"]);
                    $("#preview-cost").val(data["cost"]);
                    $("#shirt-preview-message").show();
                } else{
                    $("#shirt-error-message").show();
                    $("#shirt-preview-message").hide();
                }
            });
        }

        if( target == "#trousers-level-up-panel4" ||
            target == "#skirt-level-up-panel4" ||
            target == "#shirt-level-up-panel4" ||
            target == "#dress-level-up-panel4"){
            $("#submit-order-row").show();
        } else{
            $("#submit-order-row").hide();
        }
    });
    $("#submit-order-row").click(function(e){
        e.preventDefault();
    });

    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus();
    });

    $('#send-data-assign').click(function(e){
        e.preventDefault();

        $.post('/dashboard/orders', $('#data-assign').serialize(), function(data, status, xhr)
        {
            // do something here with response;
            $(window).attr('location','/dashboard/orders')
        })
        .done(function() {
            // do something here if done ;
        })
        .fail(function() {
            // do something here if there is an error ;
        })
        .always(function() {
        });

        $('#myModalHorizontal').modal("hide");
    });
})();



