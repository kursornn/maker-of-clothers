<?php $__env->startSection('content'); ?>
    <div class="bootstrap col-sm-10">
        <?php if(isset($user)): ?>
            <h1>Update Size</h1>
        <?php else: ?>
            <h1>Create new Size</h1>
        <?php endif; ?>
        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach($errors->all() as $error): ?>
                        <li><?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php if(!isset($size)): ?>
            <?php echo Form::open(['route' => 'dashboard.sizes.store', 'method' => 'POST', 'files'=>'true']); ?>

        <?php else: ?>
            <?php echo Form::open(['url' => '/dashboard/sizes/'.$size->id, 'method' => 'PUT', 'files'=>'true']); ?>

            <?php echo Form::hidden('id', $size->id); ?>

        <?php endif; ?>

        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <?php echo Form::text('description', isset($size)? $size->description : old('description'), ['class' => 'form-control','id'=>'description','placeholder'=>'Description']); ?>

            </div>
        </div>

        <input type="submit" class="btn btn-dark" value="Submit">
        <a href="<?php echo e(url('users')); ?>" class="btn btn-default">Cancel</a>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>