<?php $__env->startSection('content'); ?>
    <span class="right">
            <a href="tissues/create" class="btn btn-dark">New Tissue</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>Photo</th>
                <th>Description</th>
                <th>Complexity</th>
                <th>Created at</th>
                <th>Edit / Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($tissues as $tissue): ?>
                <tr class="odd gradeX">
                    <td>
                        <div class="row">
                            <div class="col-xs-2">
                                <?php if($tissue->photo): ?>
                                    <img src="<?php echo e(asset('media/users/'.$tissue->photo)); ?>" alt="" class="src" style="width: 30px; height: 30px;">
                                <?php else: ?>
                                    <img src="<?php echo e(asset('images/blog_user_blank.png')); ?>" alt=""
                                         style="width: 30px; height: 30px;">
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-10">
                                <?php echo $tissue->name; ?>

                            </div>
                        </div>
                    </td>
                    <td><?php echo $tissue->description; ?></td>
                    <td><?php echo $tissue->complexity; ?></td>
                    <td><?php echo $tissue->created_at; ?></td>
                    <td><a class="text-dark" href="tissues/<?php echo e($tissue->id); ?>/edit">Edit</a>
                        <a class="text-dark" href="tissues/<?php echo e($tissue->id); ?>/delete">Delete</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>