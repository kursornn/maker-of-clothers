<?php $__env->startSection('content'); ?>
    <span class="right">
            <a href="users/create" class="btn btn-dark">New User</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Created at</th>
                <th>Edit / Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($users as $user): ?>
                <tr class="odd gradeX">
                    <td>
                        <div class="row">
                            <div class="col-xs-2">
                                <?php if($user->avatar): ?>
                                    <img src="<?php echo e(asset('media/users/'.$user->avatar)); ?>" alt="" class="src" style="width: 30px; height: 30px;">
                                <?php else: ?>
                                    <img src="<?php echo e(asset('images/blog_user_blank.png')); ?>" alt=""
                                         style="width: 30px; height: 30px;">
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-10">
                                <?php echo $user->name; ?>

                            </div>
                        </div>
                    </td>
                    <td><?php echo $user->email; ?></td>
                    <td><?php echo $user->created_at; ?></td>
                    <td><a class="text-dark" href="users/<?php echo e($user->id); ?>/edit">Edit</a>
                        <a class="text-dark" href="users/<?php echo e($user->id); ?>/delete">Delete</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>