<?php $__env->startSection('content'); ?>
    <div class="bootstrap col-sm-10">
        <?php if(isset($user)): ?>
            <h1>Update User</h1>
        <?php else: ?>
            <h1>Create new User</h1>
        <?php endif; ?>
        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach($errors->all() as $error): ?>
                        <li><?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>


        <?php if(!isset($user)): ?>
            <?php echo Form::open(['route' => 'dashboard.users.store', 'method' => 'POST', 'files'=>'true']); ?>

        <?php else: ?>
            <?php echo Form::open(['url' => 'dashboard/users/'.$user->id, 'method' => 'PUT', 'files'=>'true']); ?>

            <?php echo Form::hidden('id', $user->id); ?>

        <?php endif; ?>

        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <?php echo Form::text('name', isset($user)? $user->name : old('name'), ['class' => 'form-control','id'=>'name','placeholder'=>'Name']); ?>

            </div>
            <div class="form-group">
                <?php echo Form::password('password', ['class' => 'form-control','id'=>'password','placeholder'=>'Password']); ?>

            </div>
            <?php /*<div class="form-group">*/ ?>
                <?php /*<?php echo Form::text('phone', isset($user)? $user->phone : old('phone'), ['class' => 'form-control','id'=>'name','placeholder'=>'Phone']); ?>*/ ?>
            <?php /*</div>*/ ?>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <div class="form-group">
                        <div class="preview-uploader">
                            <?php if(isset($user) && $user->avatar && isset($user->avatar)): ?>
                                <label for="image"
                                       style="background-image: url('<?php echo e(asset('media/users/'.$user->avatar)); ?>')">
                                    <span>Click to change image</span>
                                </label>
                            <?php else: ?>
                                <label for="image"><span>Image</span></label>
                            <?php endif; ?>

                            <input type="file" name="avatar" id="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <?php echo Form::email('email', isset($user)? $user->email : old('email'), ['class' => 'form-control','id'=>'name','placeholder'=>'Email']); ?>

            </div>
            <?php /*<div class="form-group">*/ ?>
                <?php /*<?php echo Form::select('roles[]', $roles , isset($user)?$user->present()->roles: null  , ['class' => 'form-control','id'=>'roles','multiple'=>'multiple']); ?>*/ ?>
            <?php /*</div>*/ ?>

        </div>
        <div class="row">

        </div>

        <input type="submit" class="btn btn-dark" value="Submit">
        <a href="<?php echo e(url('users')); ?>" class="btn btn-default">Cancel</a>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>