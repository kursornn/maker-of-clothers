<?php $__env->startSection('content'); ?>
    <div class="bootstrap col-sm-10">
        <?php if(isset($user)): ?>
            <h1>Update Tissue</h1>
        <?php else: ?>
            <h1>Create new Tissue</h1>
        <?php endif; ?>
        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach($errors->all() as $error): ?>
                        <li><?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>


        <?php if(!isset($tissue)): ?>
            <?php echo Form::open(['route' => 'dashboard.tissues.store', 'method' => 'POST', 'files'=>'true']); ?>

        <?php else: ?>
            <?php echo Form::open(['url' => 'dashboard/tissues/'.$tissue->id, 'method' => 'PUT', 'files'=>'true']); ?>

            <?php echo Form::hidden('id', $tissue->id); ?>

        <?php endif; ?>

        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <?php echo Form::text('description', isset($tissue)? $tissue->description : old('description'), ['class' => 'form-control','id'=>'description','placeholder'=>'Description']); ?>

                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-right">
                <div class="filter-switches nobr">
                <span class="toggle-switch-group">
                    <?php echo Form::checkbox('active',1, \Illuminate\Support\Facades\Input::get('active'), ['id' => 'unpub_check','class'=>'checkbox checkbox_dark', ($tissue->active == 0) ? "" :'checked'=> true]); ?>

                    <label for="unpub_check" class="nobr">Active</label>
                </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <?php echo Form::text('complexity', isset($tissue)? $tissue->complexity : old('complexity'), ['class' => 'form-control','id'=>'complexity','placeholder'=>'Complexity']); ?>

                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="preview-uploader">
                                <?php if(isset($tissue) && $tissue->photo && isset($tissue->photo)): ?>
                                    <label for="image"
                                           style="background-image: url('<?php echo e(asset('media/users/'.$tissue->photo)); ?>')">
                                        <span>Click to change image</span>
                                    </label>
                                <?php else: ?>
                                    <label for="image"><span>Image</span></label>
                                <?php endif; ?>
                                <input type="file" name="photo" id="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
        </div>

        <input type="submit" class="btn btn-dark" value="Submit">
        <a href="<?php echo e(url('tissues')); ?>" class="btn btn-default">Cancel</a>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>