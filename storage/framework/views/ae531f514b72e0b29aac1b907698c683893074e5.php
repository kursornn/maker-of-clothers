<?php $__env->startSection('content'); ?>
    <span class="right">
            <a href="/dashboard/sizes/create" class="btn btn-dark">New Size</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>ID</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($sizes as $size): ?>
                <tr class="odd gradeX">
                    <td><?php echo $size->id; ?></td>
                    <td>
                        <div class="row">
                            <div class="col-xs-10">
                                <?php echo $size->description; ?>

                            </div>
                        </div>
                    </td>
                    <td><?php echo $size->created_at; ?></td>
                    <td><a class="text-dark" href="sizes/<?php echo e($size->id); ?>/edit">Edit</a>
                        <a class="text-dark" href="sizes/<?php echo e($size->id); ?>/delete">Delete</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>