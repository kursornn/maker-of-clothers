<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php /*<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/ ?>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,700,300italic,400italic,600italic'
          rel='stylesheet' type='text/css'>

    <title>Maker Of Clothers</title>
    <meta name="csrf-token" id="token" content="<?php echo e(csrf_token()); ?>"/>

    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/sb-admin.css')); ?>">
<?php /*    <link rel="stylesheet" href="<?php echo e(asset('js/datepicker/datepicker.css')); ?>">*/ ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/admin/admin.screen.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/redactor.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/admin/app.css')); ?>">
    <?php /*    <link rel="stylesheet" href="<?php echo e(asset('css/screen.css')); ?>">*/ ?>

    <?php echo $__env->yieldContent('styles'); ?>

</head>

<body>
<?php echo $__env->yieldContent('all'); ?>


<script src="<?php echo e(asset('js/jquery-1.11.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/moment.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/bootstrap-datetimepicker.js')); ?>"></script>
<script src="<?php echo e(asset('js/select2.full.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/linkify/linkify.js')); ?>"></script>
<script src="<?php echo e(asset('js/linkify/linkify-string.js')); ?>"></script>
<script src="<?php echo e(asset('js/linkify/linkify-jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/redactor/redactor.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/scripts.js')); ?>"></script>

<?php echo $__env->yieldContent('scripts'); ?>

</body>
</html>


