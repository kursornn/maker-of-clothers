<?php $__env->startSection('content'); ?>
    <span class="right">
            <a href="styles/create" class="btn btn-dark">New Style</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>Photo</th>
                <th>Description</th>
                <th>Complexity</th>
                <th>Created at</th>
                <th>Edit / Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($styles as $style): ?>
                <tr class="odd gradeX">
                    <td>
                        <div class="row">
                            <div class="col-xs-2">
                                <?php if($style->photo): ?>
                                    <img src="<?php echo e(asset('media/users/'.$style->photo)); ?>" alt="" class="src" style="width: 30px; height: 30px;">
                                <?php else: ?>
                                    <img src="<?php echo e(asset('images/blog_user_blank.png')); ?>" alt=""
                                         style="width: 30px; height: 30px;">
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-10">
                                <?php echo $style->name; ?>

                            </div>
                        </div>
                    </td>
                    <td><?php echo $style->description; ?></td>
                    <td><?php echo $style->complexity; ?></td>
                    <td><?php echo $style->created_at; ?></td>
                    <td><a class="text-dark" href="styles/<?php echo e($style->id); ?>/edit">Edit</a>
                        <a class="text-dark" href="styles/<?php echo e($style->id); ?>/delete">Delete</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>