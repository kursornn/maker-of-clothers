@extends('layouts.app')
@section('content')
    <div class="bootstrap col-sm-10">
        @if(isset($author))
            <h1>Update Author</h1>
        @else
            <h1>Create new Author</h1>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @if(!isset($author))
            {!! Form::open(['route' => 'admin.authors.store', 'method' => 'POST', 'files'=>'true']) !!}
        @else
            {!! Form::open(['url' => '/admin/authors/'.$author->id, 'method' => 'PUT', 'files'=>'true']) !!}
            {!! Form::hidden('id', $author->id) !!}
        @endif

        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                {!! Form::text('name', isset($author)? $author->name : old('name'), ['class' => 'form-control','id'=>'name','placeholder'=>'Name']) !!}
            </div>
            <div class="form-group">
                {!! Form::text('phone', isset($author)? $author->phone : old('phone'), ['class' => 'form-control','id'=>'name','placeholder'=>'Phone']) !!}
            </div>
            <div class="form-group form-group-modified">
                <label for="name" class="control-label">Bio</label>
                {!! Form::textarea('bio', isset($author)? $author->bio: old('bio'), ['class' => 'form-control','id'=>'new-bio-content', 'rows' => '5']) !!}
            </div>
            <div class="form-group">
                <div class="preview-uploader square_preview">
                    @if(isset($author) && $author->avatar && isset($author->avatar))
                        <label for="avatar" style="background-image: url('{{asset('media/authors/'.$author->avatar)}}')">
                            <span>Click to change avatar</span>
                        </label>
                    @else
                        <label for="avatar"><span>Avatar</span></label>
                    @endif

                    <input type="file" name="avatar" id="avatar">
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                {!! Form::email('email', isset($author)? $author->email : old('email'), ['class' => 'form-control','id'=>'name','placeholder'=>'Email']) !!}
            </div>
            <div class="form-group">
                {!! Form::text('twitter', isset($author)? $author->twitter : old('twitter'), ['class' => 'form-control','id'=>'twitter','placeholder'=>'Twitter']) !!}
            </div>
        </div>


        <div class="row"></div>

        <div class="col-xs-6 col-sm-6 col-md-6">
            <input type="submit" class="btn btn-dark" value="Submit">
            <a href="{{ url('/admin/authors') }}" class="btn btn-default">Cancel</a>
        </div>
        {!! Form::close() !!}
    </div>
    @if(isset($author))
        <div class="bootstrap col-sm-2">
            <div class="row">
                <div class="aside-block aside-block-topmargin">
                    <a href="#delete-confirm" data-toggle="modal" class="btn btn-block btn-wide btn-dark">Delete</a>
                </div>
            </div>
        </div>
    @endif
    @if(isset($author))
        <div class="modal modal-confirm" tabindex="-1" role="dialog" id="delete-confirm">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="/admin/authors/{{$author->id}}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <i class="fa fa-exclamation-triangle"></i>

                            <h2>Are you sure you want to delete this author?</h2>
                        </div>
                        <div class="modal-footer text-center">
                            <button type="button" class="btn btn-default btn-wide" data-dismiss="modal">Close</button>
                            <input type="submit" name="delete" class="btn btn-wide btn-dark" value="Delete">
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@endsection