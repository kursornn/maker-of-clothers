@extends('layouts.app')
@section('content')
    <span class="right">
            <a href="/admin/authors/create" class="btn btn-dark">New Author</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
            @foreach($authors as $author)
                <tr class="odd gradeX">
                    <td>
                        <div class="row">
                            <div class="col-xs-2">
                                @if($author->avatar)
                                    <img class="img-responsive img-preview" src="{{ asset('media/authors/'.$author->avatar)}}" alt="" class="src">
                                @else
                                    <img src="{{ asset('images/blog_user_blank.png') }}" alt="" style="width: 30px; height: 30px;">
                                @endif
                            </div>
                            <div class="col-xs-10">
                                <div class="sk-text-vcenter">{!! $author->name !!}</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div  class="sk-text-vcenter">{!! $author->email !!}</div>
                    </td>
                    <td>
                        <div class="sk-text-vcenter">
                            <a class="text-dark" href="/admin/authors/{{$author->id}}/edit">Edit</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection