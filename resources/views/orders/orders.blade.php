@extends('layouts.app')
@section('content')
    <span class="right">
            <a href="/dashboard/orders/create" class="btn btn-dark">New Order</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>ID</th>
                <th>Юзер</th>
                <th>Размер</th>
                <th>Ткань</th>
                <th>Стиль Верх</th>
                <th>Стиль Низ</th>
                <th>Статус</th>
                <th>Дата создания</th>
            </tr>
            </thead>
            <tbody>
                @foreach($orders as $key=>$order)
                    <tr class="odd gradeX">
                        <td>{!! ++$key !!}</td>
                        <td>{!! isset($order->author) ? $order->author->name : 'none' !!}</td>
                        <td>{!! $order->size->description !!}</td>
                        <td>{!! $order->tissue->description !!}</td>
                        <td>{!! (isset($order->style_up) ? $order->style_up->description : 'none') !!}</td>
                        <td>{!! (isset($order->style_down) ? $order->style_down->description : 'none') !!}</td>
                        <td>{!! ($order->status == 1) ? "Done" : "In Progress" !!}</td>
                        <td>{!! $order->created_at !!}</td>
                        <td>
                            <a class="text-dark" href="orders/{{$order->id}}/delete">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection