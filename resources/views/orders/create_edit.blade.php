@extends('layouts.app')
@section('content')
    <div class="bootstrap col-sm-10">
        @if(isset($order))
            <h1>Update Order</h1>
        @else
            <h1>Создайте платье мечты</h1>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(!isset($order))
            {!! Form::open(['route' => 'dashboard.orders.store', 'method' => 'POST', 'files'=>'true']) !!}
        @else
            {!! Form::open(['url' => '/dashboard/orders/'.$order->id, 'method' => 'PUT', 'files'=>'true']) !!}
            {!! Form::hidden('id', $order->id) !!}
        @endif

            <div class="row">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#type-of-element-panel1">Платья</a></li>
                    <li><a data-toggle="tab" href="#type-of-element-panel2">Юбки</a></li>
                    <li><a data-toggle="tab" href="#type-of-element-panel3">Брюки</a></li>
                    <li><a data-toggle="tab" href="#type-of-element-panel4">Рубашки</a></li>
                </ul>
                <div class="tab-content">
                    <div id="type-of-element-panel1" class="tab-pane fade in active">
                        <div class="row">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#dress-level-up-panel1">Ткань</a></li>
                                <li><a data-toggle="tab" href="#dress-level-up-panel2">Стиль</a></li>
                                <li><a data-toggle="tab" href="#dress-level-up-panel3">Размер</a></li>
                                <li><a data-toggle="tab" href="#dress-level-up-panel4">Превью</a></li>
                            </ul>
                            <div class="col-md-12">
                                <div class="tab-content">
                                    <div id="dress-level-up-panel1" class="tab-pane fade in active">
                                        <div class="row">
                                            <select id="select-dress-level-up-panel1" class="image-picker show-html">
                                                @foreach($data["tissues"] as $tissue)
                                                    <option data-img-src="/media/users/{{$tissue->photo}}" value="{{$tissue->id}}">{{$tissue->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div id="dress-level-up-panel2" class="tab-pane fade in col-md-12">
                                        <div class="row">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a data-toggle="tab" href="#level-of-clother-1">Верх</a></li>
                                                <li><a data-toggle="tab" href="#level-of-clother-2">Низ</a></li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="tab-content">
                                                        <div id="level-of-clother-1" class="tab-pane fade in active">
                                                            <select id="select-style-dress-level-up-panel1" class="image-picker masonry">
                                                                @foreach($data["styles-up"] as $style)
                                                                    @if($style->macro_type == 0)
                                                                        <option data-img-src="/media/users/{{$style->photo}}" value="{{$style->id}}">  {{$style->description}}  </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div id="level-of-clother-2" class="tab-pane fade in">
                                                            <select id="select-style-dress-level-up-panel2" class="image-picker masonry">
                                                                @foreach($data["styles-down"] as $style)
                                                                    @if($style->macro_type == 0)
                                                                        <option data-img-src="/media/users/{{$style->photo}}" value="{{$style->id}}">  {{$style->description}}  </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="dress-level-up-panel3" class="tab-pane fade in">
                                        <div class="col-md-4 extra-paddings">
                                            <select class="form-control" id="size-dress-level-up-panel3">
                                                @foreach($data["sizes"] as $size)
                                                    <option value="{{$size->id}}">{{$size->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div id="dress-level-up-panel4" class="tab-pane fade in">
                                        <div class="alert alert-danger" role="alert" id="dress-error-message" style="display: none;">
                                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                            <span class="sr-only">Error:</span> К сожалению, такое сочетание сделать невозможно.
                                        </div>
                                        <div class="row" id="dress-preview-message">
                                            <div class="col-md-12">
                                                <img id="dress-preview" src="" class="img-rounded" style="width: 100%; max-width: 250px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="type-of-element-panel2" class="tab-pane fade in">
                        <div class="row">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#skirt-level-up-panel1">Ткань</a></li>
                                <li><a data-toggle="tab" href="#skirt-level-up-panel2">Стиль</a></li>
                                <li><a data-toggle="tab" href="#skirt-level-up-panel3">Размер</a></li>
                                <li><a data-toggle="tab" href="#skirt-level-up-panel4">Превью</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="skirt-level-up-panel1" class="tab-pane fade in active">
                                    <div class="row">
                                        <select id="select-level-up-panel1" class="image-picker show-html">
                                            @foreach($data["tissues"] as $tissue)
                                                <option data-img-src="/media/users/{{$tissue->photo}}" value="{{$tissue->id}}">{{$tissue->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="skirt-level-up-panel2" class="tab-pane fade in col-md-12">
                                    <div class="row">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#skirt-style-level-of-clother-1">Верх</a></li>
                                            <li><a data-toggle="tab" href="#skirt-style-level-of-clother-2">Низ</a></li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tab-content">
                                                    <div id="skirt-style-level-of-clother-1" class="tab-pane fade in active">
                                                        <select id="select-skirt-level-up-panel2" class="image-picker masonry">
                                                            @foreach($data["styles-up"] as $style)
                                                                @if($style->macro_type == 1)
                                                                    <option data-img-src="/media/users/{{$style->photo}}" value="{{$style->id}}">  {{$style->description}}  </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div id="skirt-style-level-of-clother-2" class="tab-pane fade in">
                                                        <select id="select-skirt-level-up-panel2" class="image-picker masonry">
                                                            @foreach($data["styles-down"] as $style)
                                                                @if($style->macro_type == 1)
                                                                    <option data-img-src="/media/users/{{$style->photo}}" value="{{$style->id}}">  {{$style->description}}  </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="skirt-level-up-panel3" class="tab-pane fade in">
                                    <div class="col-md-4 extra-paddings">
                                        <select id="skirt-sizes-level-up-panel3" class="form-control">
                                            @foreach($data["sizes"] as $size)
                                                <option value="{{$size->id}}">{{$size->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="skirt-level-up-panel4" class="tab-pane fade in">
                                    <div class="alert alert-danger" role="alert" id="skirts-error-message" style="display: none;">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span> К сожалению, такое сочетание сделать невозможно.
                                    </div>
                                    <div class="row" id="skirts-preview-message">
                                        <div class="col-md-12">
                                            <img id="skirt-preview" src="" class="img-rounded">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="type-of-element-panel3" class="tab-pane fade in">
                        <div class="row">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#trousers-level-up-panel1">Ткань</a></li>
                                <li><a data-toggle="tab" href="#trousers-level-up-panel2">Стиль</a></li>
                                <li><a data-toggle="tab" href="#trousers-level-up-panel3">Размер</a></li>
                                <li><a data-toggle="tab" href="#trousers-level-up-panel4">Превью</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="trousers-level-up-panel1" class="tab-pane fade in active">
                                    <div class="row">
                                        <select id="select-level-up-panel1" class="image-picker show-html">
                                            @foreach($data["tissues"] as $tissue)
                                                <option data-img-src="/media/users/{{$tissue->photo}}" value="{{$tissue->id}}">{{$tissue->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="trousers-level-up-panel2" class="tab-pane fade in col-md-12">
                                    <div class="row">
                                        <div class="row">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a data-toggle="tab" href="#trousers-level-of-clother-1">Верх</a></li>
                                                <li><a data-toggle="tab" href="#trousers-level-of-clother-2">Низ</a></li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="tab-content">
                                                        <div id="trousers-level-of-clother-1" class="tab-pane fade in active">
                                                            <select id="select-trousers-level-up-panel2" class="image-picker masonry">
                                                                @foreach($data["styles-up"] as $style)
                                                                    @if($style->macro_type == 2)
                                                                        <option data-img-src="/media/users/{{$style->photo}}" value="{{$style->id}}">  {{$style->description}}  </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div id="trousers-level-of-clother-2" class="tab-pane fade in">
                                                            <select id="select-trousers-level-down-panel2" class="image-picker masonry">
                                                                @foreach($data["styles-down"] as $style)
                                                                    @if($style->macro_type == 2)
                                                                        <option data-img-src="/media/users/{{$style->photo}}" value="{{$style->id}}">  {{$style->description}}  </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="trousers-level-up-panel3" class="tab-pane fade in">
                                    <div class="col-md-4 extra-paddings">
                                        <select id="trousers-size-panel3" class="form-control">
                                            @foreach($data["sizes"] as $size)
                                                <option value="{{$size->id}}">{{$size->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="trousers-level-up-panel4" class="tab-pane fade in">
                                    <div class="alert alert-danger" role="alert" id="trousers-error-message" style="display: none;">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span> К сожалению, такое сочетание сделать невозможно.
                                    </div>
                                    <div class="row" id="trousers-preview-message">
                                        <div class="col-md-12">
                                            <img id="trousers-preview" src="" class="img-rounded">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="type-of-element-panel4" class="tab-pane fade in">
                        <div class="row">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#shirt-level-up-panel1">Ткань</a></li>
                                <li><a data-toggle="tab" href="#shirt-level-up-panel2">Стиль</a></li>
                                <li><a data-toggle="tab" href="#shirt-level-up-panel3">Размер</a></li>
                                <li><a data-toggle="tab" href="#shirt-level-up-panel4">Превью</a></li>
                            </ul>
                            <div class="tab-content col-md-12">
                                <div id="shirt-level-up-panel1" class="tab-pane fade in active">
                                    <div class="row">
                                        <select id="select-level-up-panel1" class="image-picker show-html">
                                            @foreach($data["tissues"] as $tissue)
                                                <option data-img-src="/media/users/{{$tissue->photo}}" value="{{$tissue->id}}">{{$tissue->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="shirt-level-up-panel2" class="tab-pane fade in col-md-12">
                                    <div class="row">
                                        <div class="row">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a data-toggle="tab" href="#shirt-level-of-clother-1">Верх</a></li>
                                                <li><a data-toggle="tab" href="#shirt-level-of-clother-2">Низ</a></li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="tab-content">
                                                        <div id="shirt-level-of-clother-1" class="tab-pane fade in active">
                                                            <select id="select-shirt-level-up-panel1" class="image-picker masonry">
                                                                @foreach($data["styles-up"] as $style)
                                                                    @if($style->macro_type == 3)
                                                                        <option data-img-src="/media/users/{{$style->photo}}" value="{{$style->id}}">  {{$style->description}}  </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div id="shirt-level-of-clother-2" class="tab-pane fade in">
                                                            <select id="select-shirt-level-up-panel2" class="image-picker masonry">
                                                                @foreach($data["styles-down"] as $style)
                                                                    @if($style->macro_type == 3)
                                                                        <option data-img-src="/media/users/{{$style->photo}}" value="{{$style->id}}">  {{$style->description}}  </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="shirt-level-up-panel3" class="tab-pane fade in">
                                    <div class="col-md-4 extra-paddings">
                                        <select id="shirt-size-level-up-panel3" class="form-control">
                                            @foreach($data["sizes"] as $size)
                                                <option value="{{$size->id}}">{{$size->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="shirt-level-up-panel4" class="tab-pane fade in">
                                    <div class="alert alert-danger" role="alert" id="shirt-error-message" style="display: none;">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span> К сожалению, такое сочетание сделать невозможно.
                                    </div>
                                    <div class="row" id="shirt-preview-message">
                                        <div class="col-md-12">
                                            <img id="shirt-preview" src="" class="img-rounded">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div id="submit-order-row" class="row">
            <div class="col-md-4">
                <!--<input type="submit" class="btn btn-dark" value="Подтвердить заказ">-->
                <!-- Button trigger modal -->
                <button class="btn btn-dark" data-toggle="modal" data-target="#myModalHorizontal">
                    Подтвердить заказ
                </button>
                <a href="{{ url('orders') }}" class="btn btn-default">Отменить</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Подтвердить заказ
                    </h4>
                </div>
                <!-- Modal Body -->
                <div class="modal-body">

                    <form id="data-assign" class="form-horizontal" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label  class="col-sm-2 control-label" for="inputEmail3">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name="email_assign" class="form-control" id="inputEmail3" placeholder="Email"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-2 control-label" for="inputEmail3">ФИО</label>
                            <div class="col-sm-10">
                                <input type="text" name="fio_assign" class="form-control" id="inputEmail32" placeholder="ФИО"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-2 control-label" for="inputEmail34">Телефон</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="phone_assign" id="inputEmail34" placeholder="Телефон"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="comments"  class="form-control" rows="5" placeholder="Комментарий"></textarea>
                            </div>
                        </div>

                        <div class="form-group" style="display: none;">
                            <label  class="col-sm-2 control-label" for="tissue-assign">Тип одежды</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="macro_type"  id="macro_type-assign" placeholder="Ткань"/>
                            </div>
                        </div>

                        <div class="form-group" style="display: none;">
                            <label  class="col-sm-2 control-label" for="tissue-assign">Ткань</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="tissue"  id="tissue-assign" placeholder="Ткань"/>
                            </div>
                        </div>

                        <div class="form-group" style="display: none;">
                            <label  class="col-sm-2 control-label" for="style-up-assign">Стиль верх</label>
                            <div class="col-sm-10">
                                <input type="text" name="style_up" class="form-control" id="style-up-assign" placeholder="Стиль верх"/>
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label  class="col-sm-2 control-label" for="tissue-assign">Стиль низ</label>
                            <div class="col-sm-10">
                                <input type="text" name="style_down" class="form-control" id="style-down-assign" placeholder="Стиль низ"/>
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label  class="col-sm-2 control-label" for="size-assign">Размер</label>
                            <div class="col-sm-10">
                                <input type="text" name="size" class="form-control" id="size-assign" placeholder="Размер"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Стоимость</label>
                            <div class="col-sm-10">
                                <label id="preview-cost" class="col-sm-2 control-label">{{}}</label>
                            </div>
                        </div>

                    </form>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Отменить
                    </button>
                    <button id="send-data-assign" type="button" class="btn btn-primary btn-dark">Заказать платье мечты</button>
                </div>
            </div>
        </div>
    </div>



@endsection