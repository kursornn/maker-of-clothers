@extends('layouts.app')
@section('content')
    <span class="right">
            <a href="tissues/create" class="btn btn-dark">New Tissue</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>Photo</th>
                <th>Description</th>
                <th>Complexity</th>
                <th>Cost</th>
                <th>Created at</th>
                <th>Edit / Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tissues as $tissue)
                <tr class="odd gradeX">
                    <td>
                        <div class="row">
                            <div class="col-xs-2">
                                @if($tissue->photo)
                                    <img src="{{ asset('media/users/'.$tissue->photo)}}" alt="" class="src" style="width: 30px; height: 30px;">
                                @else
                                    <img src="{{ asset('images/blog_user_blank.png') }}" alt=""
                                         style="width: 30px; height: 30px;">
                                @endif
                            </div>
                            <div class="col-xs-10">
                                {!! $tissue->name !!}
                            </div>
                        </div>
                    </td>
                    <td>{!! $tissue->description !!}</td>
                    <td>{!! $tissue->complexity !!}</td>
                    <td>{!! $tissue->cost !!}</td>
                    <td>{!! $tissue->created_at !!}</td>
                    <td><a class="text-dark" href="tissues/{{$tissue->id}}/edit">Edit</a>
                        <a class="text-dark" href="tissues/{{$tissue->id}}/delete">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection