@extends('layouts.app')
@section('content')
    <div class="bootstrap col-sm-10">
        @if(isset($user))
            <h1>Update Tissue</h1>
        @else
            <h1>Create new Tissue</h1>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(!isset($tissue))
            {!! Form::open(['route' => 'dashboard.tissues.store', 'method' => 'POST', 'files'=>'true']) !!}
        @else
            {!! Form::open(['url' => 'dashboard/tissues/'.$tissue->id, 'method' => 'PUT', 'files'=>'true']) !!}
            {!! Form::hidden('id', $tissue->id) !!}
        @endif

        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                        {!! Form::text('cost', isset($tissue)? $tissue->cost : old('cost'), ['class' => 'form-control','id'=>'cost','placeholder'=>'Cost']); !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::text('description', isset($tissue)? $tissue->description : old('description'), ['class' => 'form-control','id'=>'description','placeholder'=>'Description']) !!}
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-right">
                <div class="filter-switches nobr">
                <span class="toggle-switch-group">
                    {!!  Form::checkbox('active',1, \Illuminate\Support\Facades\Input::get('active'), ['id' => 'unpub_check','class'=>'checkbox checkbox_dark', (isset($tissue) && $tissue->active == 0) ? "" :'checked'=> true]) !!}
                    <label for="unpub_check" class="nobr">Active</label>
                </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::text('complexity', isset($tissue)? $tissue->complexity : old('complexity'), ['class' => 'form-control','id'=>'complexity','placeholder'=>'Complexity']) !!}
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="preview-uploader">
                                @if(isset($tissue) && $tissue->photo && isset($tissue->photo))
                                    <label for="image"
                                           style="background-image: url('{{asset('media/users/'.$tissue->photo)}}')">
                                        <span>Click to change image</span>
                                    </label>
                                @else
                                    <label for="image"><span>Image</span></label>
                                @endif
                                <input type="file" name="photo" id="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
        </div>

        <input type="submit" class="btn btn-dark" value="Submit">
        <a href="{{ url('tissues') }}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@endsection