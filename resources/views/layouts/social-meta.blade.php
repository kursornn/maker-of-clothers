<!-- Open Graph -->
<meta property="og:site_name" content="PwC Skyval" />
<meta property="og:url" content="{{ URL::current() }}" />
<meta property="og:title" content="{{ isset($story) ? $story->meta_page_title : '' }}" />
<meta property="og:description" content="{{ isset($story) ? $story->meta_description : '' }}" />
<meta property="og:image" content="{{ isset($story) ? URL::to('media/stories' . $story->image) : '' }}" />

<!-- Twitter cards -->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="PwC Skyval" />
<meta name="twitter:creator" content="PwC Skyval" />
<meta name="twitter:title" content="{{ isset($story) ? $story->meta_page_title : '' }}" />
<meta name="twitter:description" content="{{ isset($story) ? $story->meta_description : '' }}." />
<meta name="twitter:image" content="{{ isset($story) ? URL::to('media/stories' . $story->image) : '' }}" />