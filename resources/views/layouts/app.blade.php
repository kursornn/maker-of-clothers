@extends('default')
@section('all')
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            @if(Auth::check())
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle text-dark" data-toggle="dropdown" href="#">
                            Welcome, {{-- $currentUser->name --}}
                            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
                @if(Auth::user()->admin)
                <div class="navbar-default navbar-static-side" role="navigation">
                    <div class="navbar-collapse collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a class="text-dark" href="{!!   URL::to('/dashboard/orders')!!}"><i class="fa fa-edit fa-fw text-dark"></i>Orders</a>
                            </li>
                            <li>
                                <a class="text-dark" href="{!!   URL::to('/dashboard/styles')!!}"><i class="fa fa-edit fa-fw text-dark"></i>Styles</a>
                            </li>
                            <li>
                                <a class="text-dark" href="{!!   URL::to('/dashboard/tissues')!!}"><i class="fa fa-edit fa-fw text-dark"></i>TIssues</a>
                            </li>
                            <li>
                                <a class="text-dark" href="{!!   URL::to('/dashboard/sizes')!!}"><i class="fa fa-edit fa-fw text-dark"></i>Sizes</a>
                            </li>
                            <li>
                                <a class="text-dark" href="{!!   URL::to('/dashboard/users')!!}"><i class="fa fa-edit fa-fw text-dark"></i>Users</a>
                            </li>
                            <li>
                                <a class="text-dark" href="{!!   URL::to('/dashboard/corresponding')!!}"><i class="fa fa-edit fa-fw text-dark"></i>Corresponding</a>
                            </li>
                        </ul>
                    </div>
                </div>
                @endif
            @endif
        </nav>
        <div id="page-wrapper">
            @if(\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::pull('success') !!}</li>
                    </ul>
                </div>
            @endif
            @yield('content')
        </div><!-- end page-wrapper -->
    </div><!-- end id-wrapper -->
@endsection