<div id="header">
    <div class="container">
        <!--end header-logo-->
        <div id="header-nav">
            <!-- Static navbar -->
            <nav class="navbar navbar-default">
                <div id="header-logo" class="pull-left">
                    <a href="/">
                        <img src="/img/skyvallogo.png" alt="skyvallogo" />
                    </a>
                </div>
                <div class="pull-right">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="header-buttons">
                        <a href="#" class="btn btn-primary demo_btn demo_form" data-toggle="modal" data-target="#myModal">Demo</a>
                        <a href="https://app.skyval.com/" class="btn btn-info btn_login">Client log in</a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a data-scroll_to="aboutusl" href="/index.html#aboutus">What is Skyval?</a></li>
                        <li><a data-scroll_to="users" href="/index.html#users">How can I use it?</a></li>
                        <li><a data-scroll_to="benefits" href="/index.html#benefits">What are the key benefits?</a></li>
                        <li><a data-scroll_to="testimonials" href="/index.html#testimonials">Hear from our clients</a></li>
                        <li><a data-scroll_to="inside" href="/index.html#inside">What’s in Skyval</a></li>
                        <li><a data-scroll_to="articles" href="/index.html#articles">Fresh thinking</a></li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </nav>
        </div>
    </div>
</div>
<!--end header-->