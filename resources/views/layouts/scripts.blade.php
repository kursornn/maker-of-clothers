<script type="text/javascript" src="/js/vendor.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/image_picker.min.js"></script>

<script type="text/javascript" language="javascript" charset="utf-8">
    // <![CDATA[
    $(document).ready(function() {
        $('#market-update-btn').click(function () {
            window.location = '/market-updates';
        });
    });// ]]>
</script>
