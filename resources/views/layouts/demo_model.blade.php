<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog sk-modal-dialog-full-mobile-desktop" role="document">
        <div class="modal-content bg_blue" style="width: 100%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="/assets/img/close_white.png" alt="close"></span></button>
                <h4 class="modal-title" id="myModalLabel">Try Skyval Demo</h4>
            </div>
            <div class="modal-body">
                <form id="skyval-demo" action="" class="demo_form clearfix">
                    <div class="demo_form-header col-xs-12">
                        <!-- <h2 class="margin-top-none margin-bottom-lg b radius0 margin-top-xl"></h2> -->
                        <p>Fill in your details to get a free demo account to Skyval</p>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <div class="bg_blue row" id="demo">
                            <label class="col-md-2" for="skyval-demo-name">Name<sup>*</sup></label>
                            <div class="form-group col-md-9 col-xs-12 col-sm-10">  <input type="text" name="name" id="skyval-demo-name" class="form-control" placeholder="Name" required></div>
                            <label class="col-md-2 col-sm-3" for="skyval-demo-email">Email<sup>*</sup></label>
                            <div class="form-group col-md-9 col-xs-12 col-sm-10"><input type="text" name="email" id="skyval-demo-email" placeholder="Email" class="form-control" required></div>
                            <label class="col-md-2" for="skyval-demo-telephone">Phone<sup>*</sup></label>
                            <div class="form-group col-md-9 col-xs-12 col-sm-10"><input type="text" name="telephone" id="skyval-demo-telephone" placeholder="Telephone" class="form-control" required></div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5 col-sm-5 demo_form-info">
                        <p>Any addition information</p>
                        <textarea class="demo_form-area" name="descr" id="skyval-demo-descr" rows="4" placeholder="Your message"></textarea>
                        <div class="form-group margin-top-sm"><button type="submit" class="btn btn-info radius0 big_btn pull-right">Submit</button></div>
                    </div>
                </form>
                <div class="col-xs-12 modal-thanks">
                    <p>Thank you! We will be in touch soon.</p>
                </div>
                <div class="col-xs-12 popup-list">
                    <p><strong>Call us on</strong> 020 780 41503 / 020 780 44874 <span>|</span> <strong>Email us at </strong>  skyval@uk.pwc.com</p>
                </div>
            </div>
            <div class="modal-loader">
                <div class="modal-loader-box">
                    <img src="/assets/img/loader.gif" alt="loader">
                </div>
            </div>
        </div>
    </div>
</div>