@extends('layouts.app')
@section('content')
    <div class="bootstrap col-sm-10">
        @if(isset($style))
            <h1>Update Style</h1>
        @else
            <h1>Create new Style</h1>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @if(!isset($style))
            {!! Form::open(['route' => 'dashboard.styles.store', 'method' => 'POST', 'files'=>'true']) !!}
        @else
            {!! Form::open(['url' => 'dashboard/styles/'.$style->id, 'method' => 'PUT', 'files'=>'true']) !!}
            {!! Form::hidden('id', $style->id) !!}
        @endif
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    {!! Form::select('level', ['Низ', 'Верх'], isset($style)? $style->level : old('level'), ['class' => 'form-control','id'=>'level']) !!}
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    {!! Form::select('macro_type', ['Платья','Юбки','Брюки','Рубашки'], isset($style)? $style->macro_type : old('macro_type'), ['class' => 'form-control','id'=>'macro_type']) !!}
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::text('cost', isset($style)? $style->cost : old('description'), ['class' => 'form-control','id'=>'cost','placeholder'=>'Cost']) !!}
                </div>
            </div>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::text('description', isset($style)? $style->description : old('description'), ['class' => 'form-control','id'=>'description','placeholder'=>'Description']) !!}
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-right">
                <div class="filter-switches nobr">
                <span class="toggle-switch-group">
                    {!!  Form::checkbox('active',1, \Illuminate\Support\Facades\Input::get('active'), ['id' => 'unpub_check','class'=>'checkbox checkbox_dark', (isset($style) && $style->active == 0) ? "" :'checked'=> true]) !!}
                    <label for="unpub_check" class="nobr">Active</label>
                </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::text('complexity', isset($style)? $style->complexity : old('complexity'), ['class' => 'form-control','id'=>'complexity','placeholder'=>'Complexity']) !!}
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="preview-uploader">
                                @if(isset($style) && $style->photo && isset($style->photo))
                                    <label for="image"
                                           style="background-image: url('{{asset('media/users/'.$style->photo)}}')">
                                        <span>Click to change image</span>
                                    </label>
                                @else
                                    <label for="image"><span>Image</span></label>
                                @endif
                                <input type="file" name="photo" id="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

        </div>

        <input type="submit" class="btn btn-dark" value="Submit">
        <a href="{{ url('/dashboard/styles') }}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@endsection