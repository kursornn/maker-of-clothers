@extends('layouts.app')
@section('content')
    <span class="right">
            <a href="styles/create" class="btn btn-dark">New Style</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>Photo</th>
                <th>Description</th>
                <th>Complexity</th>
                <th>Level</th>
                <th>Тип одежды</th>
                <th>Created at</th>
                <th>Edit / Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($styles as $style)
                <tr class="odd gradeX">
                    <td>
                        <div class="row">
                            <div class="col-xs-2">
                                @if($style->photo)
                                    <img src="{{ asset('media/users/'.$style->photo)}}" alt="" class="src" style="width: 30px; height: 30px;">
                                @else
                                    <img src="{{ asset('images/blog_user_blank.png') }}" alt=""
                                         style="width: 30px; height: 30px;">
                                @endif
                            </div>
                            <div class="col-xs-10">
                                {!! $style->name !!}
                            </div>
                        </div>
                    </td>
                    <td>{!! $style->description !!}</td>
                    <td>{!! $style->complexity !!}</td>
                    <td>{!! (isset($style->level) && $style->level == 1) ? "Верх" : "Низ" !!}</td>
                    <th>
                        @if($style->macro_type == 0)
                            Платье
                        @elseif($style->macro_type == 1)
                            Юбка
                        @elseif($style->macro_type == 2)
                            Брюки
                        @elseif($style->macro_type == 3)
                            Рубашки
                        @else
                            FUCK
                        @endif</th>
                    <td>{!! $style->created_at !!}</td>
                    <td><a class="text-dark" href="styles/{{$style->id}}/edit">Edit</a>
                        <a class="text-dark" href="styles/{{$style->id}}/delete">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection