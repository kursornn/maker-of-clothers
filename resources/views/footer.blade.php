<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Skyval is a single platform multi-user system, which gives you control and transforms the management of your defined benefit pension schemes.</p>
                <ul class="footer-custom-link">
                    <li><a href="#">Legal and Regulatory</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <p>Tel: 020 7804 1503</p>
                <p>Email: skyval@uk.pwc.com</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <ul class="social-links">
                    <li><a href="https://www.linkedin.com/company/skyval" target="_blank" title="LinkedIn"><img src="{{ asset('img/linked.png') }}" alt="LinkedIn"></a></li>
                    <li><a href="https://twitter.com/skyvalonline" target="_blank" title="Twitter"><img src="{{ asset('img/twitter.png') }}" alt="twitter"></a></li>
                    <li><a href="https://www.youtube.com/user/SkyvalOnline" target="_blank" title="Youtube"><img src="{{ asset('img/youtube.png') }}" alt="youtube"></a></li>
                    <li><a href="https://plus.google.com/+Skyval/videos" target="_blank" title="GooglePlus"><img src="{{ asset('img/gplus.png') }}" alt="google"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>