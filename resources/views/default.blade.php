<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{--<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">--}}
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,700,300italic,400italic,600italic'
          rel='stylesheet' type='text/css'>

    <title>Maker Of Clothers</title>
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}"/>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sb-admin.css') }}">
{{--    <link rel="stylesheet" href="{{ asset('js/datepicker/datepicker.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/admin/admin.screen.css') }}">
    <link rel="stylesheet" href="{{ asset('css/redactor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/image_picker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/app.css') }}">
    {{--    <link rel="stylesheet" href="{{ asset('css/screen.css') }}">--}}

    @yield('styles')

</head>

<body>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
@yield('all')


<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('js/select2.full.min.js') }}"></script>
<script src="{{asset('js/linkify/linkify.js')}}"></script>
<script src="{{asset('js/linkify/linkify-string.js')}}"></script>
<script src="{{asset('js/linkify/linkify-jquery.min.js')}}"></script>
<script src="{{asset('js/redactor/redactor.min.js')}}"></script>
<script src="{{asset('js/image_picker.min.js')}}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>

@yield('scripts')

</body>
</html>


