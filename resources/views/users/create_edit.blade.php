@extends('layouts.app')
@section('content')
    <div class="bootstrap col-sm-10">
        @if(isset($user))
            <h1>Update User</h1>
        @else
            <h1>Create new User</h1>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @if(!isset($user))
            {!! Form::open(['route' => 'dashboard.users.store', 'method' => 'POST', 'files'=>'true']) !!}
        @else
            {!! Form::open(['url' => 'dashboard/users/'.$user->id, 'method' => 'PUT', 'files'=>'true']) !!}
            {!! Form::hidden('id', $user->id) !!}
        @endif

        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                {!! Form::text('name', isset($user)? $user->name : old('name'), ['class' => 'form-control','id'=>'name','placeholder'=>'Name']) !!}
            </div>
            <div class="form-group">
                {!!  Form::password('password', ['class' => 'form-control','id'=>'password','placeholder'=>'Password']) !!}
            </div>
            {{--<div class="form-group">--}}
                {{--{!! Form::text('phone', isset($user)? $user->phone : old('phone'), ['class' => 'form-control','id'=>'name','placeholder'=>'Phone']) !!}--}}
            {{--</div>--}}
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <div class="form-group">
                        <div class="preview-uploader">
                            @if(isset($user) && $user->avatar && isset($user->avatar))
                                <label for="image"
                                       style="background-image: url('{{asset('media/users/'.$user->avatar)}}')">
                                    <span>Click to change image</span>
                                </label>
                            @else
                                <label for="image"><span>Image</span></label>
                            @endif

                            <input type="file" name="avatar" id="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                {!! Form::email('email', isset($user)? $user->email : old('email'), ['class' => 'form-control','id'=>'name','placeholder'=>'Email']) !!}
            </div>
            {{--<div class="form-group">--}}
                {{--{!!  Form::select('roles[]', $roles , isset($user)?$user->present()->roles: null  , ['class' => 'form-control','id'=>'roles','multiple'=>'multiple']) !!}--}}
            {{--</div>--}}

        </div>
        <div class="row">

        </div>

        <input type="submit" class="btn btn-dark" value="Submit">
        <a href="{{ url('users') }}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@endsection