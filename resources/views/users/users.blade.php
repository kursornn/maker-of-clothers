@extends('layouts.app')
@section('content')
    <span class="right">
            <a href="users/create" class="btn btn-dark">New User</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Created at</th>
                <th>Edit / Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="odd gradeX">
                    <td>
                        <div class="row">
                            <div class="col-xs-2">
                                @if($user->avatar)
                                    <img src="{{ asset('media/users/'.$user->avatar)}}" alt="" class="src" style="width: 30px; height: 30px;">
                                @else
                                    <img src="{{ asset('images/blog_user_blank.png') }}" alt=""
                                         style="width: 30px; height: 30px;">
                                @endif
                            </div>
                            <div class="col-xs-10">
                                {!! $user->name !!}
                            </div>
                        </div>
                    </td>
                    <td>{!! $user->email !!}</td>
                    <td>{!! $user->created_at !!}</td>
                    <td><a class="text-dark" href="users/{{$user->id}}/edit">Edit</a>
                        <a class="text-dark" href="users/{{$user->id}}/delete">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection