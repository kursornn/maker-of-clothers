@extends('layouts.app')
@section('content')
    <div class="bootstrap col-sm-10">
        @if(isset($style))
            <h1>Update Style</h1>
        @else
            <h1>Create new Style</h1>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @if(!isset($style))
            {!! Form::open(['route' => 'dashboard.corresponding.store', 'method' => 'POST', 'files'=>'true']) !!}
        @else
            {!! Form::open(['url' => 'dashboard/corresponding/'.$style->id, 'method' => 'PUT', 'files'=>'true']) !!}
            {!! Form::hidden('id', $style->id) !!}
        @endif

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row extra-paddings">
            <div class="col-md-3">
                <p>Тип одежды</p>
                <select class="form-control" name="macro_type">
                    <option value="0">Платья</option>
                    <option value="1">Юбки</option>
                    <option value="2">Брюки</option>
                    <option value="3">Рубашки</option>
                </select>
            </div>
            <div class="col-md-3">
                <p>Ткань</p>
                <select class="form-control" name="tissue">
                    @foreach($data["tissues"] as $tisse)
                        <option value="{!! $tisse->id !!}">{!! $tisse->description !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <p>Стиль Верх</p>
                <select class="form-control" name="style_up">
                    @foreach($data["styles-up"] as $style_up)
                        <option value="{!! $style_up->id !!}">{!! $style_up->description !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <p>Стиль Низ</p>
                <select class="form-control" name="style_down">
                    @foreach($data["styles-down"] as $style_up)
                        <option value="{!! $style_up->id !!}">{!! $style_up->description !!}</option>
                    @endforeach
                </select>
            </div>
        </div>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="preview-uploader">
                                @if(isset($corresponding) && $corresponding->photo && isset($corresponding->photo))
                                    <label for="image"
                                           style="background-image: url('{{asset('media/users/'.$tissue->photo)}}')">
                                        <span>Click to change image</span>
                                    </label>
                                @else
                                    <label for="image"><span>Image</span></label>
                                @endif
                                <input type="file" name="photo" id="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <input type="submit" class="btn btn-dark" value="Submit">
        <a href="{{ url('/dashboard/styles') }}" class="btn btn-default">Cancel</a>

        {!! Form::close() !!}
    </div>
@endsection