@extends('layouts.app')
@section('content')
    <span class="right">
            <a href="/dashboard/corresponding/create" class="btn btn-dark">New Order</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>ID</th>
                <th>Тип одежды</th>
                <th>Ткань</th>
                <th>Стиль Верх</th>
                <th>Стиль Низ</th>
            </tr>
            </thead>
            <tbody>
                @foreach($data as $corresponding)
                    <tr class="odd gradeX">
                        <td>{!!$corresponding["id"]!!}</td>
                        <td>
                            @if($corresponding["macro_type"] == 0)
                                Платье
                            @elseif($corresponding["macro_type"] == 1)
                                Юбка
                            @elseif($corresponding["macro_type"] == 2)
                                Брюки
                            @elseif($corresponding["macro_type"] == 3)
                                Рубашки
                            @else
                                FUCK
                            @endif</td>
                        <td>
                            <div class="row">
                                <div class="col-xs-2">
                                    @if($corresponding["tissue"]->photo)
                                        <img src="{{ asset('media/users/'.$corresponding["tissue"]->photo)}}" alt="" class="src" style="width: 30px; height: 30px;">
                                    @else
                                        <img src="{{ asset('images/blog_user_blank.png') }}" alt=""
                                             style="width: 30px; height: 30px;">
                                    @endif
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-xs-2">
                                    @if($corresponding["style_up"]->photo)
                                        <img src="{{ asset('media/users/'.$corresponding["style_up"]->photo)}}" alt="" class="src" style="width: 30px; height: 30px;">
                                    @else
                                        <img src="{{ asset('images/blog_user_blank.png') }}" alt=""
                                             style="width: 30px; height: 30px;">
                                    @endif
                                </div>
                            </div>
                        </td>

                        <td>
                            <div class="row">
                                <div class="col-xs-2">
                                    @if($corresponding["style_down"]->photo)
                                        <img src="{{ asset('media/users/'.$corresponding["style_down"]->photo)}}" alt="" class="src" style="width: 30px; height: 30px;">
                                    @else
                                        <img src="{{ asset('images/blog_user_blank.png') }}" alt=""
                                             style="width: 30px; height: 30px;">
                                    @endif
                                </div>
                            </div>
                        </td>

                        <td>
                            <div class="row">
                                <div class="col-xs-2">
                                    @if($corresponding["photo"])
                                        <img src="{{ asset('media/corresponding/'.$corresponding["photo"])}}" alt="" class="src" style="width: 30px; height: 30px;">
                                    @else
                                        <img src="{{ asset('images/blog_user_blank.png') }}" alt=""
                                             style="width: 30px; height: 30px;">
                                    @endif
                                </div>
                            </div>
                        </td>

                        <td><!--<a class="text-dark" href="corresponding/{{ $corresponding["id"] }}/edit">Edit</a>-->
                            <a class="text-dark" href="corresponding/{{ $corresponding["id"] }}/delete">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection