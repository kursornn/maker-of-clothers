@extends('layouts.app')
@section('content')
    <span class="right">
            <a href="/dashboard/sizes/create" class="btn btn-dark">New Size</a>
    </span>
    <div class="clearfix"></div>
    <div class="table-responsive">
        <table class="table table-hover smd-table" id="dataTables-example">
            <thead>
            <tr>
                <th>ID</th>
                <th>Value</th>
                <th>Description</th>
                <th>Cost</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sizes as $size)
                <tr class="odd gradeX">
                    <td>{!! $size->id !!}</td>
                    <td>
                        {!! $size->value !!}
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-xs-10">
                                {!! $size->description !!}
                            </div>
                        </div>
                    </td>
                    <td>
                        {!! $size->cost !!}
                    </td>
                    <td>{!! $size->created_at !!}</td>
                    <td><a class="text-dark" href="sizes/{{$size->id}}/edit">Edit</a>
                        <a class="text-dark" href="sizes/{{$size->id}}/delete">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection