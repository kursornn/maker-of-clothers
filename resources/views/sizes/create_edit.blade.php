@extends('layouts.app')
@section('content')
    <div class="bootstrap col-sm-10">
        @if(isset($user))
            <h1>Update Size</h1>
        @else
            <h1>Create new Size</h1>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(!isset($size))
            {!! Form::open(['route' => 'dashboard.sizes.store', 'method' => 'POST', 'files'=>'true']) !!}
        @else
            {!! Form::open(['url' => '/dashboard/sizes/'.$size->id, 'method' => 'PUT', 'files'=>'true']) !!}
            {!! Form::hidden('id', $size->id) !!}
        @endif

        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                {!! Form::text('value', isset($size)? $size->value : old('value'), ['class' => 'form-control','id'=>'Value','placeholder'=>'Value']) !!}
            </div>
            <div class="form-group">
                {!! Form::text('description', isset($size)? $size->description : old('description'), ['class' => 'form-control','id'=>'description','placeholder'=>'Description']) !!}
            </div>
            <div class="form-group">
                {!! Form::text('cost', isset($size)? $size->cost : old('cost'), ['class' => 'form-control','id'=>'cost','placeholder'=>'Cost']) !!}
            </div>
        </div>

        <input type="submit" class="btn btn-dark" value="Submit">
        <a href="{{ url('users') }}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@endsection