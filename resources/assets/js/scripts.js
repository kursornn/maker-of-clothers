(function () {

    $(function () {

        $(document).on('change', '.preview-uploader input', function (event) {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(event.target).siblings('label').addClass('loaded')
                        .css('background-image', 'url(' + e.target.result + ')');
                };
                reader.readAsDataURL(this.files[0]);
            }
        });

        $('#campaigns, .js_select2').select2();

        $('#new-post-content').on('keyup', setTweetCount);
        if ($('#new-post-content').length) {
            setTweetCount.apply(document.getElementById('new-post-content'));
        }


        $('.js_datepicker').datetimepicker({
            sideBySide: false,
            format: "D MMMM YYYY",
            defaultDate: new Date()
        });

        $('#filter').on('submit', function (e) {

            var query = {
                campaigns: []
            };

            $.each($(this).serializeArray(), function (i, val) {

                if (val.name == "campaigns[]") {
                    query.campaigns.push(val.value);
                } else {
                    query[val.name] = val.value;
                }

            });

            query.campaigns = query.campaigns.join(',');
            //query.excluded  = query.excluded.join(',');

            //if(!query.unpublished) {
            //    query.unpublished = 0;
            //}

            if (!query.campaigns.length) {
                delete query.campaigns;
            }

            if (!query.search.length) {
                delete query.search;
            }

            if (Object.keys(query).length) {
                window.location.href = "/admin/posts?" + $.param(query);
            } else {
                window.location.href = "/admin/posts";
            }

            e.preventDefault();
            return false;
        });

        $('#active_filt').on('click', function (e) {
            e.preventDefault();
            window.location.href = "/campaigns?archived=0";
        });
        $('#archive_filt').on('click', function (e) {
            e.preventDefault();
            window.location.href = "/campaigns?archived=1";
        });


    });


    $('.checkbox-check').on('submit', function (e) {
        var $check = $(this).find('input.confirm');
        var $label = $check.parent();
        if($check.prop('checked') == false) {
            $label.addClass('shake');
            setTimeout(function () {
                $label.removeClass('shake');
            }, 1000);
            e.preventDefault();
            return false;
        }
    });


    function getTweetLength(text) {
        var linksSize = linkify.find(text).length * 23;
        var textWithLinks = text.linkify();
        textWithLinks = textWithLinks.replace(/<a(\s[^>]*)?>.*?<\/a>/ig, "");
        return (textWithLinks.length + linksSize);
    }

    function setTweetCount() {
        var message = $(this).val();
        var len = getTweetLength(message);
        $('#char-count').text(len);
        if (len > 140) {
            $('#char-count-block').addClass('oversized');
        } else {
            $('#char-count-block').removeClass('oversized');
        }
    }

    $('#new-post-content').redactor({
        buttons: ['html','formatting', 'bold', 'italic', 'link'],
        formatting: ['blockquote'],
        minHeight: 300,
        toolbarFixed: false,
        linebreaks: true,
        paragraphy: false
    });
    
})();