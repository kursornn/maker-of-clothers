// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var sass =            require('gulp-sass'),
    autoprefixer =    require('gulp-autoprefixer'),
    cssnano =         require('gulp-cssnano'),
    rename =          require('gulp-rename'),
    concat =          require('gulp-concat'),
    uglify =          require('gulp-uglify'),
    livereload =      require('gulp-livereload'),
    plumber =         require('gulp-plumber'),
    imagemin =        require('gulp-imagemin'),
    pngcrush =        require('imagemin-pngcrush'),
    mainBowerFiles =  require('main-bower-files'),
    filter =          require('gulp-filter'),
    clean =           require('gulp-clean');
    favicons =        require('gulp-favicons');
    rucksack =        require('gulp-rucksack'),
    compass =         require('gulp-compass');


// == Clean Tasks == //
gulp.task('clean-tmp', function(){
    return gulp.src('../../public/tmp/', {read: false})
      .pipe(clean({force: true}));
});
gulp.task('clean-scripts', function(){
    return gulp.src('../../public/js/*.js', {read: false})
      .pipe(clean({force: true}));
});
gulp.task('clean-styles', function(){
    return gulp.src('../../public/css/*.css', {read: false})
      .pipe(clean({force: true}));
});
gulp.task('clean-images', function(){
    return gulp.src('../../public/img/*', {read: false})
      .pipe(clean({force: true}));
});


// == STYLES TASKS == //

// = Only compiles SASS and autoprefixes = //
gulp.task('styles-dev', function() {
  return gulp.src('scss/*.scss')
      .pipe(plumber())
    .pipe(sass({ style: 'expanded' }))
    .pipe(rucksack())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
    .pipe(gulp.dest('../../public/css/'));
});
// =
gulp.task('styles-admin-dev', function() {
    gulp.src('./sass_admin/*.scss')
        .pipe(compass({
            config_file: 'config.rb',
            css: '../../public/css/',
            sass: 'sass'
        }))
        .pipe(gulp.dest('../../public/css/'));

    gulp.src('css/*.css')
        .pipe(plumber())
        .pipe(gulp.dest('../../public/css/'));
});
// = Compiles SASS, autoprefixes then minifies the final version = //
gulp.task('styles-build', function() {
  return gulp.src('scss/*.scss')
      .pipe(plumber())
    .pipe(sass({ style: 'expanded' }))
    .pipe(rucksack())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
    .pipe(gulp.dest('../../public/css/'))
    .pipe(cssnano())
    .pipe(gulp.dest('../../public/css/'));
});

// == SCRIPTS TASKS == //

// = Only copies over the javascript files and concatinates them = //
gulp.task('scripts-dev',function(){
  return gulp.src(['js/swipe.js','js/app.js'])
      .pipe(plumber())
    .pipe(concat('main.js'))
    .pipe(gulp.dest('../../public/js/'));
});

gulp.task('scripts-admin-dev',function(){
    return gulp.src(['js/video.js.map','js/bootstrap-datetimepicker.js', 'js/bootstrap.min.js', 'js/jquery-1.11.1.min.js ', 'js/jquery.slimscroll.min.js', 'js/modal.js', 'js/moment.min.js', 'js/scripts.js', 'js/select2.full.min.js'])
        .pipe(plumber())
        .pipe(gulp.dest('../../public/js/'));
});
// = Uglifies the javascript files then concatinates them = //
gulp.task('scripts-build', function() {
  return gulp.src(['js/swipe.js','js/app.js'])
    .pipe(plumber())
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('../../public/js/'));
});
// = Uglifies the javascript files then concatinates them = //
gulp.task('scripts-admin-build', function() {
    return gulp.src(['js/bootstrap-datetimepicker.js', 'js/bootstrap.min.js', 'js/jquery-1.11.1.min.js ', 'js/jquery.slimscroll.min.js', 'js/modal.js', 'js/moment.min.js', 'js/scripts.js', 'js/select2.full.min.js'])
        .pipe(plumber())
        .pipe(gulp.dest('../../public/js/'));
});
// = Fonts Dev = //
gulp.task('fonts-dev', function() {
  return gulp.src('fonts/**')
    .pipe(plumber())
    .pipe(gulp.dest('../../public/fonts/'))
});
// = Fonts Build = //
gulp.task('fonts-build', function() {
  return gulp.src('fonts/**')
    .pipe(plumber())
    .pipe(gulp.dest('../../public/fonts/'));
});

// == VENDOR TASKS == // 

// = Copies and concatinates the development vendor CSS and JS specified in Bower (Read the Docs) = //
gulp.task('vendor-dev', function(){
  gulp.src(mainBowerFiles())
    .pipe(plumber())
    .pipe(filter('*.js'))
    .pipe(gulp.dest('../../public/tmp/'))
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('../../public/js/'));
  gulp.src(mainBowerFiles())
    .pipe(plumber())
    .pipe(filter('*.css'))
    .pipe(gulp.dest('../../public/tmp/'))
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('../../public/css/'));
});
// = Same thing as vendor-dev except we'll uglify the Javascript and cssnano the CSS = //
gulp.task('vendor-build', function() {
  gulp.src(mainBowerFiles())
    .pipe(plumber())
    .pipe(filter('*.js'))
    .pipe(gulp.dest('../../public/tmp/'))
    .pipe(uglify())
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('../../public/js/'));
  gulp.src(mainBowerFiles())
    .pipe(plumber())
    .pipe(filter('*.css'))
    .pipe(gulp.dest('../../public/tmp/'))
    .pipe(cssnano())
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('../../public/css/'));
});

// == IMAGES TASKS == //

// = Any images in the assets/img folder are minified then copied over to the public/img folder = //
gulp.task('imageminify', function () {
  return gulp.src('img/**')
      /* .pipe(imagemin({
          progressive: true,
          svgoPlugins: [{removeViewBox: false}],
          use: [pngcrush()]
      }))*/
      .pipe(gulp.dest('../../public/img/'));
});

// == VIDEO TASKS == //
gulp.task('video', function () {
  return gulp.src('video/*')
      .pipe(gulp.dest('../../public/video/'));
});


// = Apple Favicons Generate = //
gulp.task('favicons-generator', function () {
    gulp.src('assets/img/favicon.png').pipe(favicons({
      // Icon Types
      android: true,
      apple: true,
      appleStartup: true,
      coast: true,
      favicons: true,
      firefox: true,
      opengraph: false,
      windows: true,
      yandex: true,

      // Miscellaneous
      html: '../../public/tmp/favicons.html',
      background: 'transparent',
      display: "browser",
      appName: "Face-to-Cars",
      tileBlackWhite: false,
      manifest: null,
      trueColor: false,
      logging: true
    })).pipe(gulp.dest('../../public/img/favicons/'));
});


// == WATCH TASKS == //

// = Watches all SASS, JS, and the image folder for any changes, then runs the appropriate task. 
// = Also watches all PHP, CSS, JS and the image folder in the dist folder for any changes then triggers livereload
gulp.task('watch', function() {
  gulp.watch(['scss/**/*.scss'], ['styles-dev']);
  gulp.watch('sass_admin/**/*.scss', ['styles-admin-dev']);
  gulp.watch('js/**/*.js', ['scripts-dev', 'scripts-admin-dev']);

  livereload.listen();
  gulp.watch('../../public/css/*.css').on('change', livereload.changed);
  gulp.watch('../../public/js/*.js').on('change', livereload.changed);
  gulp.watch('../../public/img/**').on('change', livereload.changed);
});


// == GULP TASKS == //

// = Clean Task = //
gulp.task('clean', ['clean-styles', 'clean-scripts']);
// = Image Task = //
gulp.task('imagemin', ['imageminify']);
// = Development Task = //
gulp.task('dev', ['clean', 'fonts-dev', 'vendor-dev', 'styles-dev', 'styles-admin-dev', 'scripts-dev', 'scripts-admin-dev', 'imagemin']);
// = Build Task = //
gulp.task('build', ['clean', 'fonts-build', 'vendor-build', 'styles-build', 'scripts-build', 'scripts-admin-build', 'imagemin', 'video']);
// = Favicons Task = //
gulp.task('favicons', ['favicons-generator']);
// = Default Task = //
gulp.task('default', ['dev', 'watch']);