<?php
use Illuminate\Support\Facades\Input;

function hasRadioInput($inputName)
{
    if ( !Input::has($inputName))
        return false;

    return (int)Input::get($inputName);

}