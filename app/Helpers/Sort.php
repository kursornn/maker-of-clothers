<?php

function sort_by_two($array, $col1, $col2)
{
    $sorted = array_sort($array, function ($value) use ($col1, $col2) {
        return sprintf('%s,%s', $value[$col1], $value[$col2]);
    });

    return $sorted;
}

function sort_by($column, $body, $request)
{


    $sortBy = Session::get('sortBy');

    if ($request->has('sortBy')) {
        $sortBy = $request->get('sortBy');
        Session::put('sortBy', $sortBy);
    }

    Session::get('order') == 'asc' ? $direction = 'desc' : $direction = 'asc';


    if ($request->has('order')) {
        $request->get('order') == 'asc' ? $direction = 'desc' : $direction = 'asc';
        Session::put('order', $request->get('order'));
    }
    if ($column == $sortBy) {
        $sort_icon = 'fa-sort-numeric-'.$direction;
    } else {
        $sort_icon = 'fa-sort-numeric-asc';
    }

    $object = URL::current();
    if ($column == $sortBy) {

    }
    $html = '<a class="filter-sort-heading" href="' . $object . '?sortBy=' . $column . '&order=' . $direction . '">' . $body . ' <i class="fa '.$sort_icon.'"></i></a>';


    return $html;
}