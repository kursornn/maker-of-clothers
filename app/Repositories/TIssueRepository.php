<?php


namespace App\Repositories;


use App\Models\TIssue;

class TIssueRepository extends Repository
{
    protected $createFromFields = ['active', 'photo', 'description', 'complexity', 'cost'];
    protected $updateFromFields = ['active', 'photo', 'description', 'complexity', 'cost'];

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(TIssue $tissue)
    {
        $this->object = $tissue;
    }

}