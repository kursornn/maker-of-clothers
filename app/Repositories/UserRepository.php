<?php


namespace App\Repositories;


use App\Models\User;
use Illuminate\Support\Str;

class UserRepository extends Repository
{
    protected $createFromFields = ['name', 'email', 'avatar', 'password'];
    protected $updateFromFields = ['name', 'email', 'avatar', 'password'];

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->object = $user;
    }

}