<?php


namespace App\Repositories;


use App\Models\Size;

class SizeRepository extends Repository
{
    protected $createFromFields = ['description', 'value', 'cost'];
    protected $updateFromFields = ['description', 'value', 'cost'];

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(Size $size_data)
    {
        $this->object = $size_data;
    }

}