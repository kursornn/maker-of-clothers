<?php


namespace App\Repositories;


use App\Models\Style;
use App\Models\User;
use Illuminate\Support\Str;

class StyleRepository extends Repository
{
    protected $createFromFields = ['level', 'cost', 'active', 'description', 'photo', 'complexity', 'macro_type'];
    protected $updateFromFields = ['level', 'cost', 'active', 'description', 'photo', 'complexity', 'macro_type'];

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(Style $style)
    {
        $this->object = $style;
    }

    public function getAllForUpLevel($columns = ['*'])
    {
        return $this->object->where('level', 1)->get($columns);
    }

    public function getAllForDownLevel($columns = ['*'])
    {
        return $this->object->where('level', 0)->get($columns);
    }
}