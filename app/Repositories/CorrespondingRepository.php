<?php


namespace App\Repositories;


use App\Models\Corresponding;
use App\Models\User;

class CorrespondingRepository extends Repository
{
    protected $createFromFields = ['macro_type', 'tissue', 'style_up', 'style_down', 'photo'];
    protected $updateFromFields = ['macro_type', 'tissue', 'style_up', 'style_down', 'photo'];

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(Corresponding $corresponding)
    {
        $this->object = $corresponding;
    }

    public function preview($macro_type, $tissue, $style_up, $style_down){
        $matchThese = ["macro_type" => $macro_type, "tissue" =>$tissue, "style_up" =>$style_up, "style_down" => $style_down];
        return $this->object->where($matchThese)->first();
    }
}