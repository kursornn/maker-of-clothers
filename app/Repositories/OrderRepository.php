<?php


namespace App\Repositories;

use App\Models\Order;

class OrderRepository extends Repository
{
    protected $createFromFields = ['author', 'size', 'tissue', 'macro_type', 'style_up', 'style_down', 'email_assign', 'fio_assign', 'phone_assign'];
    protected $updateFromFields = [];

    /**
     * UserRepository constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->object = $order;
    }

    public function getOrderFor($userId, $columns = ['*']){
        return $this->object->where("author", $userId)->orderBy('created_at','DESC')->get($columns);
    }
}