<?php

namespace App\Handlers;


interface CroppableContract
{
    public function getFolder();
    public function getFilename();
    public function getCropSizes();
}