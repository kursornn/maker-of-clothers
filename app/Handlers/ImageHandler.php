<?php


namespace App\Handlers;


use File;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageHandler
{

    public function uploadImage(UploadedFile $file, $folder, $includePath = false)
    {
        $relativePath = 'media/' . $folder . '/';
        $filename     = time() . '-' . clean_string($file->getClientOriginalName());

        if ( !File::exists(public_path($relativePath))) {
            $this->createDirIfNotExists(public_path('media'));
            File::makeDirectory(public_path($relativePath));
        }
        $path = public_path($relativePath);
        $file->move($path, $filename);

        if ($includePath)
            return asset($relativePath . $filename);

        return $filename;
    }

    public function replaceImage($oldImage, $newImage, $folder)
    {

        $this->deleteImage($oldImage, $folder);

        $filename = $this->uploadImage($newImage, $folder);

        return $filename;
    }

    public function checkForUpdate($object, $image, $folder, $thumbs = false, $fieldName = 'image')
    {
        $nImage = is_array($object->{$fieldName}) ? $object->{$fieldName}['name'] : $object->{$fieldName};

        if ($image instanceof UploadedFile) {

            if ( !$nImage) {
                $checkedImage = $this->uploadImage($image, $folder);
            } else {
                if ($thumbs)
                    $this->deleteAllThumbs($object);
                $checkedImage = $this->replaceImage($nImage, $image, $folder);
            }
        } else {

            $checkedImage = explode('/', $nImage);
            $checkedImage = end($checkedImage);

        }

        return $checkedImage;

    }

    public function deleteImage($image)
    {
        if (File::exists(public_path($image))) {
            File::delete(public_path($image));
        }
    }

    public function remove($oldImage, $folder)
    {
        $relativePath = 'media/' . $folder . '/' . $oldImage;
        if (File::exists(public_path($relativePath))) {
            File::delete(public_path($relativePath));
        }
    }

    public function moveFromTemp($image, $destinationFolder)
    {
        $this->moveImageToFolder('temp', $destinationFolder, $image);
    }

    public function moveImageToFolder($from, $to, $image)
    {
        $initialPath = public_path('media/' . $from . '/' . $image);

        $destinationPath = public_path('media/' . $to . '/' . $image);

        $this->createDirIfNotExists(public_path('media/' . $to . '/'));
        File::move($initialPath, $destinationPath);
    }

    public function createDirIfNotExists($dir)
    {
        if ( !File::exists($dir)) {
            File::makeDirectory($dir);
        }
    }

    public function prepareImageUsingTemp($image, $destinationFolder)
    {
        if ($image['temp'] && $image['name'] && !$image['delete']) {
            $this->remove($image['oldImage'], $destinationFolder);
            $this->moveFromTemp($image['name'], $destinationFolder);
        }
        if ($image['delete']) {
            if ($image['temp']) {
                $this->remove($image['name'], 'temp');
            } else {
                $this->remove($image['name'], $destinationFolder);
            }
            $image['name'] = null;
        }

        return $image;
    }

    public function resizeImage($ini_path, $dest_path, $params = array ())
    {
        $width        = if_array_key_exists('width', $params);
        $height       = if_array_key_exists('height', $params);
        $constraint   = if_array_key_exists('constraint', $params);
        $rgb          = if_array_key_exists('rgb', $params, 0xFFFFFF);
        $quality      = if_array_key_exists('quality', $params, 100);
        $aspect_ratio = if_array_key_exists('aspect_ratio', $params, true);
        $crop         = if_array_key_exists('crop', $params, true);

        if ( !file_exists($ini_path)) return false;

        if ( !is_dir($dir = dirname($dest_path))) mkdir($dir);

        $img_info = getimagesize($ini_path);
        if ($img_info === false) return false;

        $ini_p = $img_info[0] / $img_info[1];
        if ($constraint) {
            $con_p  = $constraint['width'] / $constraint['height'];
            $calc_p = $constraint['width'] / $img_info[0];

            if ($ini_p < $con_p) {
                $height = $constraint['height'];
                $width  = $height * $ini_p;
            } else {
                $width  = $constraint['width'];
                $height = $img_info[1] * $calc_p;
            }
        } else {
            if ( !$width && $height) {
                $width = ($height * $img_info[0]) / $img_info[1];
            } else if ( !$height && $width) {
                $height = ($width * $img_info[1]) / $img_info[0];
            } else if ( !$height && !$width) {
                $width  = $img_info[0];
                $height = $img_info[1];
            }
        }

        preg_match('/\.([^\.]+)$/i', basename($dest_path), $match);
        $ext           = $match[1];
        $output_format = ($ext == 'jpg') ? 'jpeg' : $ext;

        $format = strtolower(substr($img_info['mime'], strpos($img_info['mime'], '/') + 1));
        $icfunc = "imagecreatefrom" . $format;

        $iresfunc = "image" . $output_format;

        if ( !function_exists($icfunc)) return false;

        $dst_x = $dst_y = 0;
        $src_x = $src_y = 0;
        $res_p = $width / $height;
        if ($crop && !$constraint) {
            $dst_w = $width;
            $dst_h = $height;
            if ($ini_p > $res_p) {
                $src_h = $img_info[1];
                $src_w = $img_info[1] * $res_p;
                $src_x = ($img_info[0] >= $src_w) ? floor(($img_info[0] - $src_w) / 2) : $src_w;
            } else {
                $src_w = $img_info[0];
                $src_h = $img_info[0] / $res_p;
                $src_y = ($img_info[1] >= $src_h) ? floor(($img_info[1] - $src_h) / 2) : $src_h;
            }
        } else {
            if ($ini_p > $res_p) {
                $dst_w = $width;
                $dst_h = $aspect_ratio ? floor($dst_w / $img_info[0] * $img_info[1]) : $height;
                $dst_y = $aspect_ratio ? floor(($height - $dst_h) / 2) : 0;
            } else {
                $dst_h = $height;
                $dst_w = $aspect_ratio ? floor($dst_h / $img_info[1] * $img_info[0]) : $width;
                $dst_x = $aspect_ratio ? floor(($width - $dst_w) / 2) : 0;
            }
            $src_w = $img_info[0];
            $src_h = $img_info[1];
        }

        $isrc  = $icfunc($ini_path);
        $idest = imagecreatetruecolor($width, $height);
        if (($format == 'png' || $format == 'gif') && $output_format == $format) {
            imagealphablending($idest, false);
            imagesavealpha($idest, true);
            imagefill($idest, 0, 0, IMG_COLOR_TRANSPARENT);
            imagealphablending($isrc, true);
            $quality = 0;
        } else {
            imagefill($idest, 0, 0, $rgb);
        }
        imagecopyresampled($idest, $isrc, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
        $res = $iresfunc($idest, $dest_path, $quality);

        imagedestroy($isrc);
        imagedestroy($idest);

        return $res;
    }

    public static function resize($ini_path, $dest_path, $params = [])
    {

        $imgToResize  = Image::make($ini_path);
//        $resizeWidth  = $params['width'];
//        $resizeHeight = null;
//
//        if ($imgToResize->width() > $imgToResize->height()) {
//            $resizeWidth  = null;
//            $resizeHeight = $params['height'];
//        }

        $imgToResize->resize($params['width'], null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $extra = 0;
//        if ($resizeWidth == null) {
//
//            $canvWidth = round(($params['width']) / 100);
//            $ext       = $imgToResize->width() - $params['width'];
//
//            if ($ext < $canvWidth) {
//                $extra = $canvWidth - $ext;
//            }
//            $imgToResize->resizeCanvas(-$canvWidth + $extra, 0, 'top-right', true);
//
//            $newCanvWidth = $imgToResize->width() - $params['width'];
//            $imgToResize->resizeCanvas(-$newCanvWidth, 0, 'top-left', true);
//        } else {
//            $canvHeight = round(($params['height']) / 100);
//            $ext        = $imgToResize->width() - $params['height'];
//
//            if ($ext < $canvHeight) {
//                $extra = $canvHeight - $ext;
//            }
//            $imgToResize->resizeCanvas(0, -(int)$canvHeight + $extra, 'bottom', true);
//
//            $newcanvHeight = $imgToResize->height() - $params['height'];
//
//            $imgToResize->resizeCanvas(0, -$newcanvHeight, 'top', true);
//        }

        $imgToResize->save($dest_path);


        return $imgToResize;
    }

    public function saveAllThumbs(CroppableContract $object)
    {
        $filename = $object->getFilename();
        if ( !$filename)
            return;
        $path = public_path('media/' . $object->getFolder() . '/');
        foreach ($object->getCropSizes() as $width => $height) {
            $params = array ('width' => $width, 'height' => $height, 'aspect_ratio' => true, 'crop' => true);

            if (File::exists($path . $width . 'x' . $height . '/')) {
                $this->resize($path . $filename, $path . $width . 'x' . $height . '/' . $filename, $params);
            } else {
                File::makeDirectory($path . $width . 'x' . $height . '/');
                $this->resize($path . $filename, $path . $width . 'x' . $height . '/' . $filename, $params);
            }
        }


    }

    private function deleteAllThumbs(CroppableContract $object)
    {
        $filename = $object->getFilename();
        $path     = public_path('media/' . $object->getFolder() . '/');
        foreach ($object->getCropSizes() as $width => $height) {
            if (File::exists($path . $width . 'x' . $height . '/' . $filename)) {
                File::delete($path . $width . 'x' . $height . '/' . $filename);
            }
        }
    }
}