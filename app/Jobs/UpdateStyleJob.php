<?php

namespace App\Jobs;

use App\Handlers\ImageHandler;
use App\Jobs\Job;
use App\Repositories\StyleRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateStyleJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $level;
    public $cost;
    public $active;
    public $description;
    public $photo;
    public $complexity;
    public $style;
    public $macro_type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($style, $level, $cost, $active, $description, $photo, $complexity, $macro_type)
    {
        $this->level = $level;
        $this->style = $style;
        $this->cost = $cost;
        $this->active = $active;
        $this->description = $description;
        $this->photo = $photo;
        $this->complexity = $complexity;
        $this->macro_type = $macro_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(StyleRepository $repo, ImageHandler $handler)
    {
        $this->photo = $handler->checkForUpdate($this->style, $this->photo, 'users', false, 'photo');

        $style = $repo->updateFromJob($this->style, $this);

        return $style;
    }
}
