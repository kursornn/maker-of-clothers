<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Repositories\SizeRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateSizeJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $description;
    public $value;
    public $cost;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($description, $value, $cost)
    {
        $this->description = $description;
        $this->value = $value;
        $this->cost = $cost;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SizeRepository $sizeRepository)
    {
        $user = $sizeRepository->createFromJob($this);

        return $user;
    }
}
