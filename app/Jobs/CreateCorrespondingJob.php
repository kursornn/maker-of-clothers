<?php

namespace App\Jobs;

use App\Handlers\ImageHandler;
use App\Jobs\Job;
use App\Repositories\CorrespondingRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateCorrespondingJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $macro_type;
    public $tissue;
    public $style_up;
    public $style_down;
    public $photo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($macro_type,$tissue,$style_up,$style_down,$photo){
        $this->macro_type = $macro_type;
        $this->tissue = $tissue;
        $this->style_up = $style_up;
        $this->style_down = $style_down;
        $this->photo = $photo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CorrespondingRepository $repo, ImageHandler $handler)
    {
        if ($this->photo != null) {
            $this->photo = $handler->uploadImage($this->photo, 'corresponding');
        }

        $corresponding = $repo->createFromJob($this);
        return $corresponding;
    }
}
