<?php

namespace App\Jobs;

use App\Handlers\ImageHandler;
use App\Jobs\Job;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $name;
    public $email;
    public $password;
    public $avatar;

    /**
     * Create a new job instance.
     *
     * @param $name
     * @param $email
     * @param $avatar
     * @param $password
     */
    public function __construct($name, $email, $avatar, $password)
    {
        $this->name     = $name;
        $this->email    = $email;
        $this->password = $password;
        $this->avatar   = $avatar;
    }

    /**
     * Execute the job.
     *
     * @param UserRepository $users
     * @param ImageHandler $handler
     * @return \App\Models\User
     */
    public function handle(UserRepository $users, ImageHandler $handler)
    {
        if ($this->avatar != null) {
            $this->avatar = $handler->uploadImage($this->avatar, 'users');
        }
        $this->password = bcrypt($this->password);

        $user = $users->createFromJob($this);

        return $user;
    }
}
