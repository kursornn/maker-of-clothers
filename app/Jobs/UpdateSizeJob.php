<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Repositories\SizeRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateSizeJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $size;
    public $description;
    public $value;
    public $cost;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($size, $description, $value, $cost)
    {
        $this->size = $size;
        $this->description = $description;
        $this->value = $value;
        $this->cost = $cost;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SizeRepository $sizeRepository)
    {
        $size = $sizeRepository->updateFromJob($this->size, $this);
        return $size;
    }
}
