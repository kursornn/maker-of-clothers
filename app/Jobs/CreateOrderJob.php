<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Repositories\OrderRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrderJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $email_assign;
    public $fio_assign;
    public $phone_assign;
    public $macro_type;
    public $tissue;
    public $style_up;
    public $style_down;
    public $size;
    public $author;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($author, $email_assign, $fio_assign, $phone_assign, $macro_type, $tissue, $style_up, $style_down, $size)
    {
        $this->email_assign = $email_assign;
        $this->fio_assign = $fio_assign;
        $this->phone_assign = $phone_assign;
        $this->macro_type = $macro_type;
        $this->tissue = $tissue;
        $this->style_up = $style_up;
        $this->style_down = $style_down;
        $this->size = $size;
        $this->author = $author;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrderRepository $repo)
    {
        $order = $repo->createFromJob($this);
        return $order;
    }
}
