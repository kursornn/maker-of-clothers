<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Repositories\CorrespondingRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateCorrespondingJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;
    public $macro_type;
    public $tissue;
    public $style_up;
    public $style_down;
    public $photo;
    public $corresponding;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($corresponding, $macro_type,$tissue,$style_up,$style_down,$photo){
        $this->macro_type = $macro_type;
        $this->tissue = $tissue;
        $this->style_up = $style_up;
        $this->style_down = $style_down;
        $this->photo = $photo;
        $this->corresponding = $corresponding;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CorrespondingRepository $repo, ImageHandler $handler)
    {
        $this->photo = $handler->checkForUpdate($this->corresponding, $this->photo, 'corresponding', false, 'photo');
        $corresponding = $repo->updateFromJob($this->style, $this);

        return $corresponding;
    }
}
