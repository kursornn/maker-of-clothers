<?php

namespace App\Jobs;

use App\Handlers\ImageHandler;
use App\Jobs\Job;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $password;
    public $user;
    public $roles;
    public $name;
    public $email;
    public $avatar;
    public $phone;

    public function __construct($user, $name, $email, $avatar, $password)
    {
        $this->password = $password;
        $this->user     = $user;
        $this->name     = $name;
        $this->email    = $email;
        $this->avatar   = $avatar;
    }


    public function handle(UserRepository $repo, ImageHandler $handler)
    {
        if ($this->password)
            $this->password = bcrypt($this->password);
        else
            $this->password = $this->user->password;
        $this->avatar = $handler->checkForUpdate($this->user, $this->avatar, 'users', false, 'avatar');

        $user = $repo->updateFromJob($this->user, $this);

//        $user->roles()->detach();
//        if ($this->roles)
//            $user->roles()->attach($this->roles);

        return $user;
    }
}
