<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Repositories\TIssueRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Handlers\ImageHandler;

class CreateTIssueJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $active;
    public $photo;
    public $description;
    public $complexity;
    public $cost;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($active, $photo, $description, $complexity, $cost)
    {
        $this->active = $active;
        $this->photo  = $photo;
        $this->description = $description;
        $this->complexity  = $complexity;
        $this->cost  = $cost;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TIssueRepository $tissueRepository, ImageHandler $handler)
    {
        if ($this->photo != null) {
            $this->photo = $handler->uploadImage($this->photo, 'users');
        }

        $tissue = $tissueRepository->createFromJob($this);
        
        return $tissue;
    }
}
