<?php

namespace App\Jobs;

use App\Handlers\ImageHandler;
use App\Jobs\Job;
use App\Repositories\TIssueRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateTIssueJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $tissue;
    public $active;
    public $photo;
    public $description;
    public $complexity;
    public $cost;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tissue, $active, $photo, $description, $complexity, $cost)
    {
        $this->tissue = $tissue;
        $this->active = $active;
        $this->photo = $photo;
        $this->description = $description;
        $this->complexity = $complexity;
        $this->cost = $cost;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TIssueRepository $tissueRepository, ImageHandler $handler)
    {
        $this->photo = $handler->checkForUpdate($this->tissue, $this->photo, 'users', false, 'photo');

        $tissue = $tissueRepository->updateFromJob($this->tissue, $this);


        return $tissue;
    }
}
