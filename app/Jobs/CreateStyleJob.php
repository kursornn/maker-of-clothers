<?php

namespace App\Jobs;

use App\Handlers\ImageHandler;
use App\Jobs\Job;
use App\Repositories\StyleRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateStyleJob extends Job implements SelfHandling
{
    use InteractsWithQueue, SerializesModels;

    public $level;
    public $cost;
    public $active;
    public $description;
    public $photo;
    public $complexity;
    public $macro_type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($level, $cost, $active, $description, $photo, $complexity, $macro_type)
    {
        $this->level = $level;
        $this->cost = $cost;
        $this->active = $active;
        $this->description = $description;
        $this->photo = $photo;
        $this->complexity = $complexity;
        $this->macro_type = $macro_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(StyleRepository $repo, ImageHandler $handler)
    {
        if ($this->photo != null) {
            $this->photo = $handler->uploadImage($this->photo, 'users');
        }
        $style = $repo->createFromJob($this);

        return $style;
    }
}
