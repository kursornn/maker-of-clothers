<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Jobs\CreateUserJob;
use App\Jobs\UpdateUserJob;
use App\Models\User;
use App\Repositories\UserRepository;
use Collective\Bus\Dispatcher;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminUserController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * AdminUserController constructor.
     * @param UserRepository $users
     * @param Dispatcher $dispatcher
     */
    public function __construct(UserRepository $users, Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->users      = $users;
    }

    public function index()
    {
        $users = $this->users->allPaginate(50);
        return view('users.users', compact('users'));
    }

    public function create()
    {
        return view('users.create_edit');
    }

    public function store(CreateUserRequest $request)
    {
        $this->dispatcher->dispatchFrom(CreateUserJob::class, $request);

        return redirect()->to('dashboard/users')->with('success', 'User has been created');

    }

    public function edit(User $user)
    {
        return view('users.create_edit', compact('user'));
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        $request['user'] = $user;

        $this->dispatcher->dispatchFrom(UpdateUserJob::class, $request);

        return redirect()->to('dashboard/users')->with('success', 'User has been updated');
    }

    public function destroy(User $user)
    {
        $this->users->delete($user);

        return redirect()->back();
    }

}
