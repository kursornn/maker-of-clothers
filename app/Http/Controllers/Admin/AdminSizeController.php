<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateSizeRequest;
use App\Http\Requests\UpdateSizeRequest;
use App\Jobs\CreateSizeJob;
use App\Jobs\UpdateSizeJob;
use App\Models\Size;
use App\Repositories\SizeRepository;
use Collective\Bus\Dispatcher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminSizeController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;
    /**
     * @var UserRepository
     */
    private $sizes;

    /**
     * AdminSizeController constructor.
     * @param SizeRepository $users
     * @param Dispatcher $dispatcher
     */
    public function __construct(SizeRepository $sizes, Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->sizes      = $sizes;
    }

    public function index()
    {
        $sizes = $this->sizes->allPaginate(50);
        return view('sizes.sizes', compact('sizes'));
    }

    public function create()
    {
        return view('sizes.create_edit');
    }

    public function store(CreateSizeRequest $request)
    {
        $this->dispatcher->dispatchFrom(CreateSizeJob::class, $request);

        return redirect()->to('dashboard/sizes')->with('success', 'Size has been created');
    }

    public function edit($id, SizeRepository $sizeRepository)
    {
        $size = $sizeRepository->find($id);
        return view('sizes.create_edit', compact('size'));
    }

    public function update($id, SizeRepository $sizeRepository, UpdateSizeRequest $request)
    {
        $request['size'] = $sizeRepository->find($id);
        $this->dispatcher->dispatchFrom(UpdateSizeJob::class, $request);

        return redirect()->to('dashboard/sizes')->with('success', 'Size has been updated');
    }

    public function destroy(Size $size)
    {
        $this->sizes->delete($size);

        return redirect()->to('dashboard/sizes')->with('success', 'Size has been deleted');
    }
}
