<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\CreateOrderJob;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Repositories\SizeRepository;
use App\Repositories\StyleRepository;
use App\Repositories\TIssueRepository;
use App\Repositories\UserRepository;
use Illuminate\Container\Container;
use Collective\Bus\Dispatcher;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;
    /**
     * @var OrdersRepository
     */
    private $orepo;
    private $urepo;
    private $srepo;
    private $tirepo;
    private $sizerepo;

    /**
     * AdminUserController constructor.
     * @param UserRepository $users
     * @param Dispatcher $dispatcher
     */
    public function __construct(OrderRepository $orepo,
                                UserRepository $urepo,
                                StyleRepository $srepo,
                                SizeRepository $sizerepo,
                                TIssueRepository $tirepo,
                                Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->orepo     = $orepo;
        $this->urepo     = $urepo;
        $this->tirepo     = $tirepo;
        $this->srepo     = $srepo;
        $this->sizerepo     = $sizerepo;
    }

    public function index()
    {
        $defaultOrders = $this->orepo->getOrderFor(Auth::user()->id);

        if(Auth::user()->admin){
            $defaultOrders = $this->orepo->getAll();
        }

        $orders = [];
        foreach($defaultOrders as $order){
            $temp = $order;
            $temp->author = $this->urepo->find($order->author);
            $temp->size = $this->sizerepo->find($order->size);
            $temp->tissue = $this->tirepo->find($order->tissue);
            $temp->style_up = $this->srepo->find($order->style_up);
            $temp->style_down = $this->srepo->find($order->style_down);
            $orders[] = $temp;
        }

        return view('orders.orders', compact('orders'));
    }

    public function create()
    {
        $order = [];
        $tissues = $this->tirepo->getAll();
        $sizes = $this->sizerepo->getAll();

        $data["tissues"] = $tissues;
        $data["styles-down"] = $this->srepo->getAllForDownLevel();
        $data["styles-up"] = $this->srepo->getAllForUpLevel();
        $data["sizes"] = $sizes;
        
        return view('orders.create_edit', compact('data'));
    }

    public function store(Requests\CreateOrderRequest $request)
    {
        $request["author"] = Auth::user()->id;
        $this->dispatcher->dispatchFrom(CreateOrderJob::class, $request);
        return ['success' => 'Orders has been created'];
    }



    public function destroy(Order $order)
    {
        $this->orepo->delete($order);

        return redirect()->back();
    }
}