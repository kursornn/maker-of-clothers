<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateTIssueRequest;
use App\Http\Requests\UpdateTIssueRequest;
use App\Jobs\CreateTIssueJob;
use App\Jobs\UpdateTIssueJob;
use App\Models\TIssue;
use App\Repositories\TIssueRepository;
use Collective\Bus\Dispatcher;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class AdminTIssueController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;
    /**
     * @var UserRepository
     */
    private $tissues;

    /**
     * AdminSizeController constructor.
     * @param SizeRepository $users
     * @param Dispatcher $dispatcher
     */
    public function __construct(TIssueRepository $tissues, Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->tissues      = $tissues;
    }

    public function index()
    {
        $tissues = $this->tissues->allPaginate(50);
        return view('tissues.tissues', compact('tissues'));
    }

    public function create()
    {
        return view('tissues.create_edit');
    }

    public function store(CreateTIssueRequest $request)
    {
        $this->dispatcher->dispatchFrom(CreateTIssueJob::class, $request);
        return redirect()->to('dashboard/tissues')->with('success', 'TIssue has been created');
    }

    public function edit($id, TIssueRepository $tissueRepository)
    {
        $tissue = $tissueRepository->find($id);
        return view('tissues.create_edit', compact('tissue'));
    }

    public function update($id, TIssueRepository $tissueRepository, UpdateTIssueRequest $request)
    {
        $request['tissue'] = $tissueRepository->find($id);

        $this->dispatcher->dispatchFrom(UpdateTIssueJob::class, $request);

        return redirect()->to('dashboard/tissues')->with('success', 'TIssue has been updated');
    }

    public function destroy(TIssue $tissue)
    {
        $this->tissues->delete($tissue);
        return redirect()->to('dashboard/tissues')->with('success', 'TIssue has been deleted');
    }
}
