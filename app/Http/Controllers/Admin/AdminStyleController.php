<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateStyleRequest;
use App\Http\Requests\UpdateStyleRequest;
use App\Jobs\CreateStyleJob;
use App\Jobs\UpdateStyleJob;
use App\Models\Style;
use App\Repositories\StyleRepository;
use App\Repositories\TIssueRepository;
use Collective\Bus\Dispatcher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminStyleController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;
    /**
     * @var UserRepository
     */
    private $repo;

    /**
     * AdminSizeController constructor.
     * @param SizeRepository $users
     * @param Dispatcher $dispatcher
     */
    public function __construct(StyleRepository $repo, Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->repo      = $repo;
    }

    public function index(Requests\ShowRequest $request)
    {
        $styles = $this->repo->allPaginate(50);
        return view('styles.styles', compact('styles'));
    }

    public function create(Requests\ShowRequest $request)
    {
        return view('styles.create_edit');
    }

    public function store(CreateStyleRequest $request)
    {
        $this->dispatcher->dispatchFrom(CreateStyleJob::class, $request);
        return redirect()->to('dashboard/styles')->with('success', 'Style has been created');
    }

    public function edit($id, StyleRepository $repo)
    {
        $style = $repo->find($id);
        return view('styles.create_edit', compact('style'));
    }

    public function update($id, UpdateStyleRequest $request, StyleRepository $repo)
    {
        $request['style'] = $repo->find($id);
        $this->dispatcher->dispatchFrom(UpdateStyleJob::class, $request);

        return redirect()->to('dashboard/styles')->with('success', 'Style has been updated');
    }

    public function destroy(Style $style)
    {
        $this->repo->delete($style);

        return redirect()->back();
    }
}
