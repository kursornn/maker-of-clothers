<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\CreateCorrespondingJob;
use App\Jobs\UpdateCorrespondingJob;
use App\Models\Corresponding;
use App\Repositories\CorrespondingRepository;
use App\Repositories\SizeRepository;
use App\Repositories\StyleRepository;
use App\Repositories\TIssueRepository;
use Collective\Bus\Dispatcher;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminCorrespondingController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;
    /**
     * @var UserRepository
     */
    private $repo;

    /**
     * AdminSizeController constructor.
     * @param SizeRepository $users
     * @param Dispatcher $dispatcher
     */
    public function __construct(CorrespondingRepository $repo, StyleRepository $srepo, TIssueRepository $trepo, Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->repo       = $repo;
        $this->srepo      = $srepo;
        $this->trepo      = $trepo;
    }


    public function index()
    {
        $data = [];
        $allc = $this->repo->getAll();

        foreach ($allc as $corresponding){
            $temp = [];
            $temp["id"] = $corresponding->id;
            $temp["macro_type"] = $corresponding->macro_type;
            $temp["photo"] = $corresponding->photo;

            $temp["tissue"] = $this->trepo->where('id', $corresponding->tissue);
            $temp["style_up"] = $this->srepo->where('id', $corresponding->style_up);
            $temp["style_down"] = $this->srepo->where('id', $corresponding->style_down);
            $data[] = $temp;
        }
        return view('corresponding.orders', compact('data'));
    }

    public function create()
    {
        $data["tissues"]= $this->trepo->getAll();
        $data["styles-down"] = $this->srepo->getAllForDownLevel();
        $data["styles-up"] = $this->srepo->getAllForUpLevel();
        return view('corresponding.create_edit', compact('data'));
    }

    public function store(Requests\CreateCorrespondingRequest $request)
    {
        $this->dispatcher->dispatchFrom(CreateCorrespondingJob::class, $request);
        return redirect()->to('dashboard/corresponding')->with('success', 'Style has been created');
    }

    public function edit($id, CorrespondingRepository $repo)
    {
        $data = $repo->find($id);
        return view('corresponding.create_edit', compact('data'));
    }

    public function update($id, CorrespondingRepository $repo, Requests\UpdateCorrespondingRequest $request)
    {
        $request['corresponding'] = $repo->find($id);

        $this->dispatcher->dispatchFrom(UpdateCorrespondingJob::class, $request);

        return redirect()->to('dashboard/tissues')->with('success', 'TIssue has been updated');
    }

    public function destroy(Corresponding $corresponding)
    {
        $this->repo->delete($corresponding);

        return redirect()->back();
    }

    public function preview(CorrespondingRepository $repo, Request $request, SizeRepository $srepo, TIssueRepository $trepo){
        $macro_type = $request->get("macro_type");
        $tissue = $request->get("tissue");
        $style_up = $request->get("style_up");
        $style_down = $request->get("style_down");
        $corresponding = $repo->preview($macro_type, $tissue,$style_up,$style_down);

        $size_cost = $srepo->find($request->get("size"));
        $tissue_cost = $trepo->find($tissue);

        $cost = $size_cost["cost"] + $tissue_cost["cost"];

        $data["corresponding"] = $corresponding;
        $data["cost"]          = $cost;

        return $data;
    }
}
