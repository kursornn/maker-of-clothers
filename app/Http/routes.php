<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix' => 'dashboard', 'middleware' => ['web', 'auth']], function () {
    Route::get('/', function () {
        if(Auth::user()->admin){
            return redirect()->to('/dashboard/orders');
        } else {
            return redirect()->to('/dashboard/orders/create');
        }
    });

    Route::resource('/users','Admin\AdminUserController');
    Route::get('/users/{users}/delete', 'Admin\AdminUserController@destroy');

    Route::resource('/sizes','Admin\AdminSizeController');
    Route::get('/sizes/{size}/delete', 'Admin\AdminSizeController@destroy');

    Route::resource('/tissues','Admin\AdminTIssueController');
    Route::get('/tissues/{tissue}/delete', 'Admin\AdminTIssueController@destroy');

    Route::resource('/styles','Admin\AdminStyleController');
    Route::get('/styles/{style}/delete', 'Admin\AdminStyleController@destroy');

    Route::resource('/orders','Admin\OrderController');
    Route::get('/orders/{order}/delete', 'Admin\OrderController@destroy');

    Route::get('/corresponding/preview','Admin\AdminCorrespondingController@preview');
    Route::resource('/corresponding','Admin\AdminCorrespondingController');
    Route::get('/corresponding/{corresponding}/delete', 'Admin\AdminCorrespondingController@destroy');
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/', function () {
        return redirect()->to('/dashboard/orders');
    });
});
