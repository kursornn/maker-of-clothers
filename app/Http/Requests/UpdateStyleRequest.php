<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class UpdateStyleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()->admin){
            return true;
        }
        return false;
    }

    public function sanitize()
    {
        if (!$this->has('complexity')) {
            $input['complexity'] = 0;
            $this->merge($input);
        }

        if (!$this->has('description')) {
            $input['description'] = null;
            $this->merge($input);
        }

        if (!$this->has('active')) {
            $input['active'] = 0;
            $this->merge($input);
        }

        if ( !$this->hasFile('photo')) {
            $input['photo'] = null;
            $this->merge($input);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
