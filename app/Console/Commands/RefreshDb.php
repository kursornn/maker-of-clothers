<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;

class RefreshDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refreshDb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::statement('DROP DATABASE IF EXISTS `' . getenv('DB_DATABASE') . '`');
        \DB::statement('CREATE DATABASE `' . getenv('DB_DATABASE') . '`');

        if (File::exists(storage_path('database.sqlite'))) {
            File::delete(storage_path('database.sqlite'));
            File::put(storage_path('database.sqlite'), '');
        }
        $this->comment('test');
    }
}
