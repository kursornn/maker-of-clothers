<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStyle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('styles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('level')->default(0)->nullable();
            $table->integer('cost')->default(0)->nullable();
            $table->integer('active')->default(0);
            $table->integer('macro_type')->default(0);
            $table->string('description')->nullable();
            $table->string('photo')->nullable();
            $table->string('complexity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('styles');
    }
}
