<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTissue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('active')->default(0);
            $table->string('photo')->nullable();
            $table->string('description')->nullable();
            $table->string('complexity')->default("");
            $table->integer('cost')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_issues');
    }
}