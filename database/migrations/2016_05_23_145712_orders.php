<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author')->unsigned();
            $table->integer('size')->unsigned()->nullable();
            $table->integer('tissue')->unsigned()->nullable();
            $table->integer('macro_type')->unsigned()->nullable();
            $table->integer('style_up')->unsigned()->nullable();
            $table->integer('style_down')->unsigned()->nullable();
            $table->string('email_assign')->nullable();
            $table->string('fio_assign')->nullable();
            $table->string('phone_assign')->nullable();
            $table->string('comments')->nullable();
            $table->timestamp('fitting_date')->nullable();
            $table->timestamp('estimate')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
