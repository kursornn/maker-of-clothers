<?php

use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sizes')->delete();
        
        \DB::table('sizes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'description' => 'Size 1',
                'value' => '1',
                'created_at' => '2016-05-29 10:48:49',
                'updated_at' => '2016-05-29 10:49:13',
            ),
            1 => 
            array (
                'id' => 2,
                'description' => 'Size 2',
                'value' => '2',
                'created_at' => '2016-05-29 10:48:57',
                'updated_at' => '2016-05-29 10:48:57',
            ),
            2 => 
            array (
                'id' => 3,
                'description' => 'Size 3',
                'value' => '3',
                'created_at' => '2016-05-29 10:49:01',
                'updated_at' => '2016-05-29 10:49:01',
            ),
            3 => 
            array (
                'id' => 4,
                'description' => 'Size 4',
                'value' => '4',
                'created_at' => '2016-05-29 10:49:08',
                'updated_at' => '2016-05-29 10:49:08',
            ),
        ));
        
        
    }
}
