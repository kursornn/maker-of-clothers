<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@mail.com',
                'avatar' => NULL,
                'password' => '$2y$10$ww4UbbQPTlKdBBsuMMPmrOUto7iUNyYYuTVPXWVbUkXgkfxg5cC2e',
                'phone' => '',
                'size' => NULL,
                'fake' => 0,
                'admin' => true,
                'remember_token' => 'EGwB2xHZNlC0Daep0nQUf1UbtggsErrwdlfWeEGosYwk2QGsNeg42YE5A1Gg',
                'created_at' => '2015-01-20 22:08:15',
                'updated_at' => '2016-03-20 04:12:00',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'test',
                'email' => 'test@mail.com',
                'avatar' => NULL,
                'password' => '$2y$10$Q/0NUAvl5x3Cjjmb4fcrl.cRfEejg/3wi.Io2mB0wJL.OVb5pJnIy',
                'phone' => '',
                'size' => NULL,
                'fake' => 0,
                'admin' => false,
                'remember_token' => NULL,
                'created_at' => '2016-03-19 21:35:06',
                'updated_at' => '2016-03-19 21:35:06',
            ),
        ));
        
        
    }
}
