<?php

use Illuminate\Database\Seeder;

class TIssuesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('t_issues')->delete();
        
        \DB::table('t_issues')->insert(array (
            0 => 
            array (
                'id' => 1,
                'active' => 1,
                'photo' => '1464518969-logo636.jpg',
                'description' => 'Tissue for demo 1',
                'complexity' => '1',
                'created_at' => '2016-05-29 10:49:29',
                'updated_at' => '2016-05-29 10:49:29',
            ),
            1 => 
            array (
                'id' => 2,
                'active' => 1,
                'photo' => '1464519051-1.jpeg',
                'description' => 'Tissue for demo 2',
                'complexity' => '2',
                'created_at' => '2016-05-29 10:50:51',
                'updated_at' => '2016-05-29 10:50:51',
            ),
            2 => 
            array (
                'id' => 3,
                'active' => 1,
                'photo' => '1464519060-2.jpg',
                'description' => 'Tissue for demo 3',
                'complexity' => '3',
                'created_at' => '2016-05-29 10:51:00',
                'updated_at' => '2016-05-29 10:51:00',
            ),
            3 => 
            array (
                'id' => 4,
                'active' => 1,
                'photo' => '1464519076-3.jpeg',
                'description' => 'Tissue for demo 4',
                'complexity' => '5',
                'created_at' => '2016-05-29 10:51:08',
                'updated_at' => '2016-05-29 10:51:22',
            ),
            4 => 
            array (
                'id' => 5,
                'active' => 1,
                'photo' => '1464519093-4.jpeg',
                'description' => 'Tissue for demo 5',
                'complexity' => '6',
                'created_at' => '2016-05-29 10:51:33',
                'updated_at' => '2016-05-29 10:51:33',
            ),
        ));
        
        
    }
}
