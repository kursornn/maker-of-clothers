<?php

use Illuminate\Database\Seeder;

class StylesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('styles')->delete();
        
        \DB::table('styles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'level' => 1,
                'cost' => 123,
                'active' => 1,
                'description' => 'Style for demo 1',
                'photo' => '1464519157-6.jpeg',
                'complexity' => '2',
                'macro_type' => '0',
                'created_at' => '2016-05-29 10:52:37',
                'updated_at' => '2016-05-29 10:52:37',
            ),
            1 => 
            array (
                'id' => 2,
                'level' => 0,
                'cost' => 234234,
                'active' => 1,
                'description' => 'Style for demo 2',
                'photo' => '1464519189-7.jpeg',
                'complexity' => '4',
                'macro_type' => '1',
                'created_at' => '2016-05-29 10:53:09',
                'updated_at' => '2016-05-29 10:53:09',
            ),
            2 => 
            array (
                'id' => 3,
                'level' => 1,
                'cost' => 0,
                'active' => 1,
                'description' => 'Style for demo 3',
                'photo' => '1464519201-8.jpeg',
                'macro_type' => '2',
                'complexity' => '123',
                'created_at' => '2016-05-29 10:53:21',
                'updated_at' => '2016-05-29 10:53:21',
            ),
            3 => 
            array (
                'id' => 4,
                'level' => 0,
                'cost' => 0,
                'active' => 1,
                'description' => 'Style for demo 4',
                'photo' => '1464519212-8.jpg',
                'complexity' => 'qwe',
                'macro_type' => '3',
                'created_at' => '2016-05-29 10:53:32',
                'updated_at' => '2016-05-29 10:53:32',
            ),
        ));
        
        
    }
}
